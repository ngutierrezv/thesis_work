import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os
import sys

if sys.version_info[0] >= 3:
    unicode = str

#font = {'family': 'sans',
#        'weight': 'normal',
#        'size': 18}

#matplotlib.rc('font', **font)

folder = "plots/"
file_ext = ".pdf"
lw = 0.1
do_zoom = False

spy = 365. * 24. * 3600.
msun = 1.98892e33
pc2cm = 3.0856778570831e18
cm2pc = 1e0 / pc2cm

# plot method
plot = plt.plot
plotlog = plt.semilogy

if not os.path.exists(folder):
    os.makedirs(folder)

# reactions verbatim
verbs = ["H2 -> Mg+ + e-", "Mg+ + e- -> H2", "g-- + g -> g- + g-", "g-- + g+ -> g + g-",
         "g-- + g++ -> g + g", "g- + g+ -> g + g", "g- + g++ -> g + g+", "g++ + g -> g+ + g+",
         "Mg+ + g-- -> H2 + g-", "Mg+ + g- -> H2 + g", "Mg+ + g -> H2 + g+", "Mg+ + g+ -> H2 + g++",
         "e- + g- -> g--", "e- + g -> g-", "e- + g+ -> g", "e- + g++ -> g+"]

# grain variables
var_dust = ["g" + str(i) for i in range(-2,3)]

# ions variables
var_ions = ["Mg+", "e-"] + [x for x in var_dust if x != "g0"]

# chemistry + dust
var_chem = ["Mg+", "e-", "H2"] + var_dust

# rho*beta for MHD
betas = ["(rho_" + x + ")*beta_" + x for x in var_ions]

# resistivity coefficients
etas = ["eAD", "eOhm", "eHall"] + ["eAD_tab", "eOhm_tab", "eHall_tab"]

# all hydro variables
var_names_hydro = ["xpos", "time", "Tgas", "rho", "rvx", "rvy", "rvz", "By", "Bz",
                   "energy", "zeta", "H2", "Mg+", "e-"] + var_dust \
                  + ["zeta_N", "vx", "vy", "vz", "|B|"] \
                  + etas + ["x_H2", "x_Mg+", "x_e-"] + ["x_" + x for x in var_dust] \
                  + ["cool", "heat", "ctime"] + betas + verbs \
                  + ["g_acc", "g_acc x rho", "E_K", "E_B"]

skips = ["xpos", "fname", "label", "time"] #  + ["eAD_tab", "eOhm_tab", "eHall_tab"]

# sanitize filename
def slugify(value):
    for c in list("|()?* >"):
        value = value.replace(c, "_")
    #import unicodedata
    #import re
    #value = value.decode('unicode-escape')
    #value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    #value = unicode(re.sub('[^\w\s-]', '', value).strip().lower())
    #value = unicode(re.sub('[-\s]+', '-', value))
    return value


# load files and store into dict, keys=variables, value=time evolution
def load(var_names, fname):
    print("loading " + fname)
    data = {x: [] for x in var_names}
    for row in open(fname):
        srow = row.strip()
        if srow == "":
            continue
        arow = [float(x) for x in srow.split(" ") if x != ""]
        for ii, v in enumerate(var_names):
            data[v].append(arow[ii])
    data = {k: np.array(v) for k, v in data.items()}
    data["fname"] = data["label"] =\
        fname.replace(".dat", "").replace("/output_hydro", "").replace("test_", "")
    return data


# find a data subset for key1 where key2 is equal to value
def subset(data, key1, key2, value):
    subdata = []
    for ii, vv in enumerate(data[key2]):
        if vv == value:
            subdata.append(data[key1][ii])
    return np.array(subdata)


# load data from file
data = load(var_names_hydro, "output_hydro.dat")

# get times
times = np.array(sorted(list(set(data["time"]))))
tmax = max(times)

# get the last time
time = times[-1]
# position
xdata = subset(data, "xpos", "time", time)
# density
ydata = subset(data, "rho", "time", time)

accretion = np.zeros((times.shape[0], ydata.shape[0]))

# normalize derivative of the density variation
dd = np.abs(np.gradient(ydata, xdata))
dd /= np.amax(dd)

# find where the density varies more
idx = np.argwhere(dd > 1e-4).flatten()
# find the first and the last position where density varies more
xmin = xdata[min(idx)]
xmax = xdata[max(idx)]

# load colormap
cmap = plt.get_cmap("cool")
cmap2 = plt.get_cmap("summer")
cmap3 = plt.get_cmap("autumn")

latex = '''\\documentclass[11pt,a4paper]{report}
\\usepackage{graphicx}
\\usepackage{geometry}
\\newgeometry{right={1mm}, left={10mm}, top={0mm}, bottom={0mm}}
\\begin{document}
\\noindent
'''

# loop ion variables to plot
for ivar, var in enumerate(var_names_hydro):
    if var in skips:
        continue
    fname = folder + "/plot_" + slugify(var) + file_ext
    fname_zoom = folder + "/plot_zoom_" + slugify(var) + file_ext
    print("plotting %s in %s" % (var, fname))
    total_mass = np.zeros_like(times)

    plt.clf()
    # loop on times to plot at different ages
    for itime, time in enumerate(times):
        tnorm = time / tmax
        xdata = subset(data, "xpos", "time", time)
        ydata = subset(data, var, "time", time)
        if np.amin(ydata) < 0e0:
            thlin = np.amax(np.abs(ydata)) * 1e-3
            plt.yscale('symlog', linthreshy=thlin)
            plot(xdata, ydata, color=cmap(tnorm), lw=lw)
        else:
            plt.yscale('log')
            plotlog(xdata, ydata, color=cmap(tnorm), lw=lw)

        # plot sound and Alfven speed
        if var in ["vx", "vy", "vz"]:
            kboltzmann = 1.38064852e-16  # erg/K
            pmass = 1.6726219e-24  # g
            gamma = 7./5.
            mu = 2e0
            cdata = np.sqrt(gamma * kboltzmann * np.array(subset(data, "Tgas", "time", time)) / mu / pmass)
            #plot(xdata, cdata, color=cmap(tnorm), ls=":", label="c_s")

            Bfield = np.array(subset(data, "|B|", "time", time))
            rho = np.array(subset(data, "rho", "time", time))
            cdata = Bfield / np.sqrt(rho * 4e0 * np.pi)
            #plot(xdata, cdata, color=cmap(tnorm), ls="--", label="v_A")
            if tnorm == 0e0:
                plt.legend(loc="best")

        # compare kinetic, magnetic, and internal energy
        if var in ["E_K"]:
            cdata = subset(data, "energy", "time", time)
            plot(xdata, cdata, color=cmap(tnorm), ls=":", label="E", lw=lw)
            cdata = subset(data, "E_B", "time", time)
            plot(xdata, cdata, color=cmap2(tnorm), ls="--", label="E_B", lw=lw)
            cdata = subset(data, "energy", "time", time)
            plot(xdata, cdata, color=cmap3(tnorm), ls=":", label="E", lw=lw)
            if tnorm == 0e0:
                plt.legend(loc="best")

        # check if total rho from chemistry is equal to hydro rho
        if var in ["rho"]:
            cdata = np.zeros_like(ydata)
            for vv in var_chem:
                cdata += subset(data, vv, "time", time)
            plotlog(xdata, cdata, color=cmap3(tnorm), ls="--", lw=lw, label="sum")
            if tnorm == 0e0:
                plt.legend(loc="best")

        # total mass as integral of rho along x
        if var in ["rho"]:
            cdata = subset(data, "rho", "time", time)
            total_mass[itime] = np.trapz(cdata, xdata)

        # accretion
        if var in ["rvx"]:
            cdata = subset(data, "rvx", "time", time)
            accretion[itime, :] = np.pi * (xdata - np.amax(xdata) / 2e0) * cdata

        if var in ["cool"]:
            cdata = subset(data, "heat", "time", time)
            plotlog(xdata, cdata, color=cmap3(tnorm), ls="--", lw=lw, label="heat")
            if tnorm == 0e0:
                plt.legend(loc="best")

    # add some ornamets
    plt.title(var, fontsize=20)
    plt.xlabel("$x$ / cm")
    plt.tight_layout()
    plt.savefig(fname)

    # plot mass conservation in box
    if var in ["rho"]:
        plt.clf()
        plt.plot(times/spy/1e3, (total_mass - total_mass[0]) / total_mass[0] * 1e2, marker="o")
        plt.xlabel("t/kyr")
        plt.ylabel("[M(t) - M(0)] / M(0) %")
        plt.title("total mass")
        plt.tight_layout()
        plt.savefig("plots/plot_mass.pdf")

    if var in ["rvx"]:
        plt.clf()
        thlin = np.amax(np.abs(accretion)) * 1e-5
        plt.yscale('symlog', linthreshy=thlin)
        for itime, time in enumerate(times):
            tnorm = time / tmax
            plot(xdata, accretion[itime, :] / msun * spy * 1e6 * pc2cm, color=cmap(tnorm), lw=lw)
        plot(xdata, np.zeros_like(xdata), lw=lw, ls="--", color="#aaaaaa")
        plt.title("accretion")
        plt.xlabel("x/cm")
        plt.ylabel("accretion / [Msun/Myr/pc]")
        plt.tight_layout()
        plt.savefig("plots/plot_accretion.pdf")

    # zoomed version of the plot (xmin and xmax found automatically)
    if do_zoom:
        plt.title(var + ", zoom", fontsize=20, marker="x")
        plt.xlim(xmin, xmax)
        plt.tight_layout()
        plt.savefig(fname_zoom)

    # latex code for including the figure
    latex += "\\includegraphics[width=0.48\\linewidth]{%s}\n" % fname.replace(".pdf", "")
    if do_zoom:
        latex += "\\includegraphics[width=0.48\\linewidth]{%s}\n" % fname_zoom.replace(".pdf", "")
        latex += "\\\\\n"
    else:
        if ivar % 2 == 1:
            latex += "\\\\\n"

latex += "\\includegraphics[width=0.48\\linewidth]{plots/plot_mass}\n"
latex += "\\\\\n"
latex += "\\includegraphics[width=0.48\\linewidth]{plots/plot_accretion}\n"


latex += "\\end{document}\n"

fout = open("report.tex", "w")
fout.write(latex)
fout.close()
print("all plots added in report.text, compile with")
print("pdflatex report.tex")

