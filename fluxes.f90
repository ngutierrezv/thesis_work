module fluxes
contains
  ! ******************
  ! get reaction fluxes, n(:) in g/cm3
  function getFlux(n, Tgas) result(flux)
    use commons
    use rates
    implicit none
    real*8,intent(in)::n(ncell,neq),Tgas(ncell)
    real*8::flux(ncell,nreacts),k(ncell,nreacts)
    real*8::ngas(ncell,neq)
    integer::j

    !from rhoX to number density
    do j=nvar+1,neq
       ngas(:,j) = n(:,j)*imass(j)
    end do

    !get rate coefficients
    k(:,:) = getRates(n(:,:), Tgas(:))

    !!BEGIN_FLUX
! >>>>>>>>>>>>>>>>>>>>>>>>>>>
! NOTE: This block is auto-generated
! WHEN: 2020-09-01 11:53:47
! CHANGESET: b2eba3bfee45d0064a6dc77b06b4bfbf241d3ca2
! BY: nicol@Quiltras

!H2 -> Mg+ + e-
flux(:,1) = k(:,1)*ngas(:,idx_H2)

!Mg+ + e- -> H2
flux(:,2) = k(:,2)*ngas(:,idx_Mgj)*ngas(:,idx_el)

!g-- + g -> g- + g-
flux(:,3) = k(:,3)*ngas(:,idx_gqq)*ngas(:,idx_g)

!g-- + g+ -> g + g-
flux(:,4) = k(:,4)*ngas(:,idx_gqq)*ngas(:,idx_gj)

!g-- + g++ -> g + g
flux(:,5) = k(:,5)*ngas(:,idx_gqq)*ngas(:,idx_gjj)

!g- + g+ -> g + g
flux(:,6) = k(:,6)*ngas(:,idx_gq)*ngas(:,idx_gj)

!g- + g++ -> g + g+
flux(:,7) = k(:,7)*ngas(:,idx_gq)*ngas(:,idx_gjj)

!g++ + g -> g+ + g+
flux(:,8) = k(:,8)*ngas(:,idx_gjj)*ngas(:,idx_g)

!Mg+ + g-- -> H2 + g-
flux(:,9) = k(:,9)*ngas(:,idx_Mgj)*ngas(:,idx_gqq)

!Mg+ + g- -> H2 + g
flux(:,10) = k(:,10)*ngas(:,idx_Mgj)*ngas(:,idx_gq)

!Mg+ + g -> H2 + g+
flux(:,11) = k(:,11)*ngas(:,idx_Mgj)*ngas(:,idx_g)

!Mg+ + g+ -> H2 + g++
flux(:,12) = k(:,12)*ngas(:,idx_Mgj)*ngas(:,idx_gj)

!e- + g- -> g--
flux(:,13) = k(:,13)*ngas(:,idx_el)*ngas(:,idx_gq)

!e- + g -> g-
flux(:,14) = k(:,14)*ngas(:,idx_el)*ngas(:,idx_g)

!e- + g+ -> g
flux(:,15) = k(:,15)*ngas(:,idx_el)*ngas(:,idx_gj)

!e- + g++ -> g+
flux(:,16) = k(:,16)*ngas(:,idx_el)*ngas(:,idx_gjj)

    !!END_FLUX

  end function getFlux

  !************************
  function getReactionNames() result(names)
    use commons
    implicit none
    character(len=reactionNameLen)::names(nreacts)

    !!BEGIN_NAMES
! >>>>>>>>>>>>>>>>>>>>>>>>>>>
! NOTE: This block is auto-generated
! WHEN: 2020-09-01 11:53:47
! CHANGESET: b2eba3bfee45d0064a6dc77b06b4bfbf241d3ca2
! BY: nicol@Quiltras

names(1) = "H2 -> Mg+ + e-"
names(2) = "Mg+ + e- -> H2"
names(3) = "g-- + g -> g- + g-"
names(4) = "g-- + g+ -> g + g-"
names(5) = "g-- + g++ -> g + g"
names(6) = "g- + g+ -> g + g"
names(7) = "g- + g++ -> g + g+"
names(8) = "g++ + g -> g+ + g+"
names(9) = "Mg+ + g-- -> H2 + g-"
names(10) = "Mg+ + g- -> H2 + g"
names(11) = "Mg+ + g -> H2 + g+"
names(12) = "Mg+ + g+ -> H2 + g++"
names(13) = "e- + g- -> g--"
names(14) = "e- + g -> g-"
names(15) = "e- + g+ -> g"
names(16) = "e- + g++ -> g+"
    !!END_NAMES

  end function getReactionNames

end module fluxes
