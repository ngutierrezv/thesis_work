# a bunch of common variables and parameters
from math import pi

# constants
pi43 = 4e0 * pi / 3e0
kboltzmann = 1.38064852e-16  # erg/K
echarge = 4.80320425e-10  # statC
echarge2 = echarge**2
clight = 2.9979245800e10  # cm/s
pmass = 1.6726219e-24  # g

# parameters
bulkDensity = 3e0  # g/cm3
amin = 1e-7
amax = 1e-5
pexp = -3.5  # grain size distribution power-law exponent
Zabs = 2  # min/max charge, i.e. -Zabs to +Zabs
