

# *************************
# convert to floating if possible
def float_if(arg):
    try:
        return float(arg)
    except ValueError:
        return arg


# *************************
# fill trailing spaces up to max_chars
def fill_spaces(arg, max_characters):
    return str(arg) + " " * (max_characters - len(str(arg)))


# *************************
# copy input file to input.dat
def set_mhd_input_file(fname):
    import shutil
    import os
    import sys

    finput = "input.dat"
    fbak = finput + ".bak"

    # copy to bak file if input file is already present
    if os.path.isfile(finput):
        shutil.copyfile(finput, fbak)

    if not os.path.isfile(fname):
        #print "ERROR: input file " + fname + " do not exists!"
        sys.exit()

    shutil.copyfile(fname, finput)
    #print "input file " + fname + " copied to " + finput


# *************************
# F90 style for floating values
def strF90(arg, fmt='%17.8E'):
    erg = fmt % arg
    return (erg.lower().replace("e", "d")).strip()


# *************************
# convert charge to a string, e.g. +3->+++
def strCharge(charge):
    if charge == 0:
        return ""
    if charge > 0:
        return "+" * charge
    if charge < 0:
        return "-" * abs(charge)


# *******************
# random credits
def credits():
    from random import shuffle
    data = [
        ["in Canada", "in Denmark", "on a rogue planet", "in the basement",
         "on an abandoned bus", "in your kitchen",
         "on a comet", "in the jungle", "on the Moon", "on a fancy aeroplane"],
        ["TG", "NV", "TH", "JR"],
        ["talks to", "is eating", "gives not-so-relevant advices to", "draws a sketch of",
         "meets", "takes a picture of", "discuss with", "is spying",
         "tries to put an astronaut suit to"],
        ["a funny", "an excellent", "an exquisite", "a not-so-relevant", "a timeless",
         "a cheerful", "an optimistic", "an exotic", "a suspicious", "a boring", "an unoriginal"],
        ["piece of cardboard", "muffin", "time-traveller", "Lagrangian point",
         "typo in this sentnece", "gardener", "abacus", "politician", "monkey",
         "statue of a dictator", "cosmonaut", "octopus"]]

    creds = ""
    for ll in data:
        shuffle(ll)
        creds += ll[0] + " "
    #print "random credits: " + creds.strip() + "."


# **********************
# get current git changeset
def getChangeset():
    import os
    # name of the git master file
    masterfile = "./.git/refs/heads/master"
    changeset = ("x" * 7)  # default unknown changeset
    # if git master file exists grep the changeset
    if os.path.isfile(masterfile):
        changeset = open(masterfile, "rb").read()
    return changeset.strip()


# ***********************
# get current time with format
def getCurrentTime(fmt="%Y-%m-%d %H:%M:%S"):
    import datetime
    return datetime.datetime.now().strftime(fmt)


# **********************
# get user@hostname
def getUser():
    import getpass
    import socket
    return getpass.getuser() + "@" + socket.gethostname()


# *******************************
# this function indent F90 text and remove multiple blank lines
def indentF90(text, depth=1):

    # opening and closing tokens
    tokenclose = ["end do", "end if", "end function", "end subroutine", "else if", "elseif",
                  "else", "enddo", "end module", "endif", "end type", "endtype",
                  "contains", "endfunction", "endsubroutine", "endmodule", "end program",
                  "endprogram", "end interface", "endinterface", "module procedure"]
    tokenopen = ["do ", "function", "subroutine", "contains", "else", "else if",
                 "elseif", "module", "program", "type,", "interface", "module procedure"]

    # size of offset
    offset = ("  " * 4)

    # ampersand block flag
    inamper = False

    indented_text = ""
    # loop on text lines
    for row in text.split("\n"):
        srow = row.strip()
        # check opening/closing tokens (skip comments)
        if not srow.startswith("!"):
            # check for opening tokens
            for token in tokenopen:
                if srow.startswith(token):
                    depth += 1
            # check for closing tokens
            for token in tokenclose:
                if srow.startswith(token):
                    depth -= 1

        # indent text (rstrip to avoid empty lines)
        indented_text += ((offset*depth) + srow).rstrip() + "\n"

        # check for ampersand at line end (skip comments)
        if not srow.startswith("!"):
            # increase depth if ampersand found (only if not already in ampersand block)
            if srow.endswith("&") and not inamper:
                depth += 1
                inamper = True
            # decrease depth if no more ampersand found (only if in ampersand block)
            if not srow.endswith("&") and inamper:
                depth -= 1
                inamper = False

    # remove multiple blank lines
    while "\n\n\n" in indented_text:
        indented_text = indented_text.replace("\n\n\n", "\n\n")

    return indented_text
