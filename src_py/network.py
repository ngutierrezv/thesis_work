from reaction import Reaction
import utils
import shutil
from species import Species
import sys


class Network:

    # *********************
    # class constructor reads file with network
    def __init__(self, user_options):
        from commons import Zabs
        from utils import float_if

        # default options
        self.options = {"Bx": 1e-4,
                        "dx": 1.5e18,
                        "gamma": 7./5., "mu": 2e0,
                        "network": "networks/test.ntw"}

        # replace default options with user options
        for k, v in user_options.iteritems():
            if k not in self.options:
                print "ERROR: unknown option %s!" % k
                print " available options are (case sensitive)", user_options.keys()
                sys.exit()
            self.options[k] = float_if(v)

        # get filename from input options dictionary
        filename = self.options["network"]

        print "reading chemical network from " + filename

        self.nvar_names = ["rho", "rvx", "rvy", "rvz", "By", "Bz", "E", "zeta"]
        # number of non-chemical variables
        self.nvar = len(self.nvar_names)

        # default mass dictionary to trig error
        self.mass_dict = None

        # load masses from default mass file
        self.load_mass()

        self.reactions = []
        # reading chemical network
        for row in open(filename, "rb"):
            srow = row.strip()
            if srow == "":
                continue
            if srow.startswith("#"):
                continue
            idx = len(self.reactions) + 1
            # append reaction object to reactions list
            my_r = Reaction(srow, idx, self.mass_dict)
            print my_r.verbatim
            self.reactions.append(my_r)

        self.species = []
        # loop on reactions
        for myReaction in self.reactions:
            self.species += [self.get_or_create_by_name(x.name)
                             for x in (myReaction.reactants + myReaction.products)]
        # unique list of species
        self.species = list(set(self.species))

        # sort species by name
        self.species = sorted(self.species, key=lambda xx: xx.name)

        # add reactions with dust
        self.add_dust_reactions(zabs=Zabs, use_grain_grain=True)

        # compute number of equations
        self.neq = self.nvar + len(self.species)

        print "species found: " + (" ".join([x.name for x in self.species]))

    # ********************
    # add reaction with dust (zabs is min/max charge)
    def add_dust_reactions(self, zabs=2, use_grain_grain=True):

        # prepare names
        dust_species = ["g" + utils.strCharge(z) for z in range(-zabs, zabs + 1)]

        # create objects form names
        dust_species = [self.get_or_create_by_name(x) for x in dust_species]

        dust_reactions = []
        # loop on gas+dust species (only charged)
        for sp1 in (dust_species + self.species):
            # use only singly charged gas species and dust
            if (abs(sp1.charge) != 1) and not sp1.isDust:
                continue
            # skip neutrals
            if sp1.charge == 0:
                continue
            # loop on dust species (including neutrals)
            for sp2 in dust_species:
                # bool: if both species are grains
                both_dust = (sp1.isDust and sp2.isDust)
                at_least_one_is_neutral = (sp1.charge * sp2.charge == 0)
                if not use_grain_grain and both_dust:
                    continue
                # skip non-interesting interactions
                if sp1.charge + sp2.charge > zabs:
                    continue
                if sp1.charge + sp2.charge < -zabs:
                    continue
                if both_dust and (sp1.charge == sp2.charge):
                    continue
                diff = abs(sp1.charge - sp2.charge)
                if both_dust and at_least_one_is_neutral and (diff <= 1):
                    continue

                products = None
                # determine products from interaction type
                if both_dust:
                    if at_least_one_is_neutral:
                        new_charge = int(sp1.charge / 2)
                        rr1 = "g" + utils.strCharge(new_charge)
                        rr1 = self.get_or_create_by_name(rr1)
                        dcharge = sp1.charge - new_charge
                    else:
                        # neutral grain is one product
                        rr1 = self.get_or_create_by_name("g")
                        # difference in charge
                        dcharge = sp1.charge + sp2.charge
                    # grain from difference in charge
                    rr2 = "g" + utils.strCharge(dcharge)
                    rr2 = self.get_or_create_by_name(rr2)
                    # store products
                    products = [rr1, rr2]
                else:
                    # if gas species is cation
                    if sp1.charge > 0:
                        rr1 = self.get_or_create_by_name("H2")
                        rr2 = "g" + utils.strCharge([x for x in [sp1, sp2]
                                                     if x.isDust][0].charge + 1)
                        rr2 = self.get_or_create_by_name(rr2)
                        products = [rr1, rr2]
                    # if electron stick
                    if sp1.isElectron:
                        rr2 = "g" + utils.strCharge([x for x in [sp1, sp2]
                                                     if x.isDust][0].charge - 1)
                        rr2 = self.get_or_create_by_name(rr2)
                        products = [rr2]

                # prepare reaction object
                myR = Reaction()
                myR.reactants = [sp1, sp2]
                myR.products = products
                myR.idx = len(self.reactions) + 1
                myR.update()

                # skip known reactions (i.e. same products)
                if myR.rhash in dust_reactions:
                    continue
                dust_reactions.append(myR.rhash)

                print myR.verbatim
                self.reactions.append(myR)

    # ******************
    # using name get species object from self.species or create a new object and append
    def get_or_create_by_name(self, name):
        for sp in self.species:
            if name == sp.name:
                return sp
        sp = Species(name, self.mass_dict)
        self.species.append(sp)
        return sp

    # ********************
    # load species mass from file
    def load_mass(self, fname="data/mass.dat"):

        emass = 9.10938356e-28
        pmass = 1.6726219e-24
        nmass = 1.674927471e-24
        enp = emass + pmass + nmass

        self.mass_dict = dict()
        for row in open(fname, "rb"):
            srow = row.strip()
            if srow == "":
                continue
            if srow.startswith("#"):
                continue
            srow = srow.replace("\t", " ")
            (speciesName, massExpr) = [x for x in srow.split(" ") if (x != "")]
            self.mass_dict[speciesName] = eval(massExpr)

    # ***********************
    # get fluxes
    def get_flux(self):
        flux = ""
        for rea in self.reactions:
            flux += "!" + rea.verbatim + "\n"
            flux += "flux(:," + str(rea.idx) + ") = " + rea.RHS + "\n\n"
        return flux

    # **********************
    # get all RHS
    def get_ode(self, indent=0):

        ODE = dict()
        for rea in self.reactions:
            flux = rea.flux
            for reactant in [x.fidx for x in rea.reactants]:
                if reactant not in ODE:
                    ODE[reactant] = []
                ODE[reactant].append("-" + flux)
            for product in [x.fidx for x in rea.products]:
                if product not in ODE:
                    ODE[product] = []
                ODE[product].append("+" + flux)

        sODE = ""
        for k, v in ODE.iteritems():
            sODE += (" " * indent) + "dn(:," + k + ") = " + (" &\n".join(v)) + "\n\n"

        return sODE

    # **********************
    # get F90 string with all the reaction rates
    def get_rates(self):

        rates = ""
        # loop on reactions
        for rea in self.reactions:
            rates += "!" + rea.verbatim + "\n"
            rates += "k(:," + str(rea.idx) + ") = " + str(rea.rate) + "\n\n"

        return rates

    # **********************
    def get_chem_vars(self):
        from commons import Zabs
        chem_vars = ""
        for i in range(len(self.species)):
            chem_vars += "integer,parameter::" + self.species[i].fidx + "=" \
                        + str(self.nvar + i + 1) + "\n"

        chem_vars += "integer,parameter::nreacts=" + str(len(self.reactions)) + "\n"
        chem_vars += "integer,parameter::neq=" + str(self.nvar + len(self.species)) + "\n"
        chem_vars += "integer,parameter::zmin=" + str(-Zabs) + "\n"
        chem_vars += "integer,parameter::zmax=" + str(Zabs) + "\n"
        chem_vars += "integer,parameter::ztot=" + str(2*Zabs+1) + "\n"

        return chem_vars

    # ********************
    def get_mass_vars(self):

        massdef = ("0d0," * self.nvar)
        mass = (", &\n".join([utils.strF90(x.mass) for x in self.species]))
        imass = (", &\n".join([utils.strF90(1e0 / x.mass) for x in self.species]))
        mass_list = "real*8,parameter::mass(neq) = (/" + massdef + " &\n" + mass + "/)\n"
        imass_list = "real*8,parameter::imass(neq) = (/" + massdef + " &\n" + imass + "/)\n"

        return mass_list + imass_list

    # **********************
    def get_names(self):
        from utils import fill_spaces

        character_len = 10
        names = (", &\n".join(["\"" + fill_spaces(x, character_len)
                               + "\"" for x in self.nvar_names])) + ", &\n"
        names += (", &\n".join(["\"" + fill_spaces(x.name, character_len)
                                + "\"" for x in self.species]))
        name_list = "character(len=" + str(character_len) \
                    + "),parameter::varNames(neq) = (/" + names + "/)\n"
        return name_list

    # **********************
    def get_reaction_names(self):
        rnames = ""
        for rea in self.reactions:
            rnames += "names(" + str(rea.idx) + ") = \"" + rea.verbatim + "\"\n"

        return rnames

    # **********************
    def get_non_ideal(self):
        from commons import echarge

        # init sigma
        sigma_parallel = "\n!sigma parallel\n"
        sigma_pedersen = "\n!sigma pedersen\n"
        sigma_hall = "\n!sigma Hall\n"
        inv_beta2 = "\n!compute 1/(1+beta**2)\n"
        # loop on ions
        for sp1 in self.species:
            if sp1.charge == 0:
                continue
            pres = "(" + utils.strF90(sp1.charge * echarge / sp1.mass) + ")*n(:," + sp1.fidx + ")"
            sigma_parallel += "sigma(:,idx_parallel) = sigma(:,idx_parallel) + " + pres \
                             + "*beta(:," + sp1.fidx + ")\n"
            inv_beta2 += "invBeta2(:," + sp1.fidx + ") = 1d0/(1d0+beta(:," + sp1.fidx + ")**2)\n"
            sigma_pedersen += "sigma(:,idx_pedersen) = sigma(:,idx_pedersen) + " + pres \
                             + "*beta(:," + sp1.fidx + ")*invBeta2(:," + sp1.fidx + ")\n"
            sigma_hall += "sigma(:,idx_hall) = sigma(:,idx_hall) + " + pres + "*invBeta2(:," \
                         + sp1.fidx + ")\n"

        return inv_beta2 + sigma_parallel + sigma_pedersen + sigma_hall

    # **********************
    def get_beta(self):
        from math import sqrt, pi
        from commons import echarge, pi43, amin, amax, pexp, kboltzmann, clight, pmass, Zabs
        from utils import strF90
        import numpy as np
        import matplotlib.pyplot as plt

        delta_liu = 1.3e0  # Liu+2003
        alpha_H2 = 8.06e-25  # H2 polarizability, cm3

        # consider only the interactions with H2 (i.e. charged species + H2)
        sp2 = [x for x in self.species if (x.name == "H2")][0]

        # prefactor velocity
        prev = sqrt(8e0 * kboltzmann / pi / sp2.mass)

        # distribution exponents
        p1 = pexp + 1e0
        p3 = pexp + 3e0

        # grain size distribution integral
        inv_int = 1e0 / (amax**p1 - amin**p1)

        # prefactor eqn. A3
        pre_A3 = 2.21 * pi * sqrt(alpha_H2 * echarge**2 / pmass / 2e0) * inv_int
        # prefactor eqn. 25
        pre_25 = prev * pi43 * delta_liu * p1 / p3 * inv_int
        # prefactor acrit
        pre_acrit = 0.206 / sqrt(delta_liu) * alpha_H2**.25

        # precompute limits
        amin_p1 = amin**p1
        amax_p3 = amax**p3

        # default values
        xdata = ydata = ydata_25 = ydata_A3 = None

        Rdust = "! B / rho / rate\n"
        acrit = "! critical size to switch from eqn.25 to A3 (Pinto+Galli 2008), cm\n"
        plt.clf()
        # loop on charge (only positive, since |Z|)
        for i in range(Zabs):
            # charge
            z = i + 1

            # critical size in f90 format
            acrit_expr = strF90(pre_acrit * z**.25) \
                     + " * iTgas25(:)"
            acrit += "acrit(:, " + str(z) + ") = " + acrit_expr + "\n\n"

            # avoid acrit smaller than amin
            acrit += "! avoid acrit < amin\n"
            acrit += "do i=1,ngrid\n"
            acrit += "   acrit(i, " + str(z) + ") = max(acrit(i, " \
                     + str(z) + "), " + strF90(amin) + ")\n"
            acrit += "end do\n\n"

            # rate in f90 format
            rate = " (" \
                     + strF90(pre_A3*sqrt(z)) + " * (acrit(:, " + str(z) \
                     + ")**(" + str(p1) + ") - " + strF90(amin_p1) + ") " \
                     + " &\n + (" + strF90(pre_25) + ") * sqrtT(:) * ( " \
                     + strF90(amax_p3) + " - acrit(:, " + str(z) + ")**(" \
                     + str(p3) + ")))"
            Rdust += "iRdust(:, " + str(z) + ") = Birho(:) /" + rate + "\n"

            # replace f90 with x to evaluate to plot
            rate = rate.replace("&\n", "|")
            rate = rate.replace("sqrtT(:)", "sqrt(x)")
            rate = rate.replace("acrit(:, " + str(z) + ")", "(max(" + acrit_expr + ", "
                                + str(amin) + "))")
            rate = rate.replace("iTgas25(:)", "(x**(-.25))").replace("d", "e")

            # log-spaced grid temperature
            xdata = np.logspace(0e0, 4e0, 300)
            # evaluate full rate
            ydata = [eval(rate.replace("|", "")) for x in xdata]
            # evaluate eqn.A3 in P08b
            ydata_A3 = [eval(rate.split("|")[0] + ")") for x in xdata]
            # evaluate eqn.25 in P08b
            ydata_25 = [eval("(" + rate.split("|")[1]) for x in xdata]

            # plot rates
            plt.loglog(xdata, ydata, label="$Z=\\pm" + str(z) + "$")
            plt.loglog(xdata, ydata_A3, "--", label="$Z=\\pm" + str(z) + "$, RA3")
            plt.loglog(xdata, ydata_25, ":", label="$Z=\\pm" + str(z) + "$, R25")

            # save data to file
            self.save_data_to_file(xdata, [ydata, ydata_A3, ydata_25],
                                   "figs/rate_momentum_grains_Z" + str(z) + ".dat")

        # add ornaments and save to file
        plt.legend(loc="best")
        plt.title("Momentum transfer H2 + g")
        plt.xlabel("$T$ / K")
        plt.ylabel("$R_{g,n}$ / K")
        plt.savefig("figs/rate_momentum_grains.png")

        # add some returns
        Rdust += "\n"
        acrit += "\n"

        beta = ""
        # loop on species to compute their beta
        for sp1 in self.species:

            # skip neutral species (no need for neutral - H2 collisions)
            if sp1.charge == 0:
                continue

            # prefactor for beta
            preb = sp1.charge * echarge / sp1.mass * (sp1.mass + sp2.mass) / clight

            # get default rate (grain - H2 collisions)
            Rsec = "iRdust(:, " + str(abs(sp1.charge)) + ")"

            # electron - H2 collisions
            if sp1.isElectron:
                Rsec = "Birho(:) / Re(:)"

            # cation (gas) - H2 collisions
            if sp1.charge > 0 and not sp1.isDust:
                Rsec = "Birho(:) / RMgj(:)"

            # write beta in F90 format
            beta += "beta(:, " + sp1.fidx + ") = " + utils.strF90(preb) + " * " + Rsec + "\n"

        return acrit + Rdust + beta

    # *************************
    # save xdata and ydata to file fname
    @staticmethod
    def save_data_to_file(x_data, y_data_list, fname):
        fout = open(fname, "w")
        for ii, xx in enumerate(x_data):
            yd = []
            for y_data in y_data_list:
                yd.append(str(y_data[ii]))
            fout.write(str(xx) + " " + " ".join(yd) + "\n")
        fout.close()

    # **********************
    def get_ff_sum(self):

        ffsum = ""
        for sp in self.species:
            if sp.charge == 0e0:
                continue
            if sp.isElectron:
                continue
            ffsum += "ffsum(:) = ffsum(:) + " + utils.strF90(sp.charge**2) \
                     + "*ngas(:," + sp.fidx + ")\n"
        return ffsum

    # ************************
    def get_fit_data(self):

        fit = ""
        for react in self.reactions:
            if not react.xdata:
                continue
            (sp1, sp2) = react.reactants
            # krate_xdata(:)
            if fit == "":
                fit = "real*8,parameter::krate_xmin = " + utils.strF90(min(react.xdata)) + "\n"
                fit += "real*8,parameter::krate_invdx = " \
                       + utils.strF90(1e0 / (react.xdata[1] - react.xdata[0])) + "\n"
                data = (", &\n".join([utils.strF90(x) for x in react.xdata]))
                fit += "\n!rate fit xdata\n"
                fit += "real*8,parameter::krate_xdata(" + str(len(react.xdata)) \
                       + ") = (/" + data + "/)\n"
            data = (", &\n".join([utils.strF90(x) for x in react.ydata]))
            fit += "\n!" + react.verbatim + "\n"
            fit += "real*8,parameter::krate_ydata_" + sp1.fidx + "_" + sp2.fidx + "(" \
                   + str(len(react.xdata)) + ") = (/" + data + "/)\n"

        return fit

    # *******************
    def get_option_parameters(self):
        from utils import strF90
        opt = ""
        skip = ["network"]
        for k, v in self.options.iteritems():
            if k in skip:
                continue
            if k == "dx":
                opt += "real*8,parameter::%s = %s/ncell\n" % (k, strF90(v))
            else:
                opt += "real*8,parameter::%s = %s\n" % (k, strF90(v))

        return opt

    # **********************
    # do all preprocessor operations
    def preproc(self):

        self.do_preprocess("fluxes.f90", {"FLUX": self.get_flux(),
                                          "NAMES": self.get_reaction_names()})
        self.do_preprocess("odechem.f90", {"CHEM": self.get_ode()})
        self.do_preprocess("rates.f90", {"RATES": self.get_rates()})
        self.do_preprocess("commons.f90", {"CHEMVARS": self.get_chem_vars(),
                                           "MASS": self.get_mass_vars(),
                                           "NAMES": self.get_names(),
                                           "FIT": self.get_fit_data(),
                                           "OPTION_PARAMETERS": self.get_option_parameters()})
        self.do_preprocess("nonideal.f90", {"NONIDEAL": self.get_non_ideal(),
                                            "BETA": self.get_beta()})
        self.do_preprocess("cooling.f90", {"FFCOOL": self.get_ff_sum()})

    # **********************
    # generic preprocessor operation (handles pragmas)
    @staticmethod
    def do_preprocess(fname, pragma_dict):

        # temp file
        ftmp = "temp.f90"

        # dict with blocks, key=block name, value=True inside block
        blocks = dict()

        # pragma begin and end block
        pragma_begin = "!!BEGIN_"
        pragma_end = "!!END_"

        # open to write
        fout = open(ftmp, "w")

        # open to read
        for row in open(fname, "rb"):
            srow = row.strip()

            # loop on pragmas
            for (pragma, rep) in pragma_dict.iteritems():
                # check for BEGIN block
                if srow == pragma_begin + pragma:
                    fout.write(row)
                    blocks[pragma] = True
                    fout.write("! >>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
                    fout.write("! NOTE: This block is auto-generated\n")
                    fout.write("! WHEN: " + utils.getCurrentTime() + "\n")
                    fout.write("! CHANGESET: " + utils.getChangeset() + "\n")
                    fout.write("! BY: " + utils.getUser() + "\n\n")
                    if blocks[pragma]:
                        fout.write(rep)
                # check for END block
                if srow == pragma_end + pragma:
                    blocks[pragma] = False

            # if at least one block is true: skip content
            if any(blocks.values()):
                continue

            fout.write(row)
        fout.close()

        # check if all pragmas are properly closed
        if any(blocks.values()):
            print {k: v for (k, v) in blocks.iteritems() if v}
            sys.exit("ERROR: pragma still open in " + fname)

        # store original as a BAK file
        fback = fname.replace(".f90", ".bak")
        shutil.copyfile(fname, fback)

        # copy temporary file to original
        shutil.copyfile(ftmp, fname)

        print fname + " preprocessed (original stored in " + fback + ")!"
