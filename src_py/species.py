

class Species:

    # *******************
    # constructor
    def __init__(self, string, mass_dict):
        from commons import bulkDensity, pi43, amin, amax, pexp

        # store name
        self.name = string.strip()

        # compute charge
        self.charge = self.name.count("+") - self.name.count("-")

        neutral_name = self.name.replace("+", "").replace("-", "")
        if neutral_name == "g":
            p1 = pexp + 1e0
            p4 = pexp + 4e0
            # integrate rate and normalize to distribution integral
            self.mass = pi43 * bulkDensity * (amax**p4 - amin**p4) / (amax**p1 - amin**p1) * p1 / p4
            # removes the electron mass depending on charge
            self.mass -= self.charge * mass_dict["e-"]
            self.isDust = True
        else:
            # store mass from dictionary
            self.mass = mass_dict[self.name]
            self.isDust = False

        # prepare F90-safe name
        varsafe = string.strip().replace("+", "j").replace("-", "q")
        self.isElectron = False
        if self.name.lower() in ["e", "e-"]:
            self.isElectron = True
            varsafe = "el"
        self.fidx = "idx_" + varsafe
