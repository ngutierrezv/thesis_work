from species import Species
import numpy as np
from math import pi, sqrt, exp, log10


class Reaction:

    # *******************
    def __init__(self, row=None, idx=None, mass_dict=None):

        # skip empty rows
        if row is None:
            return

        # some attribute defaults to trig errors
        self.RHS = self.flux = self.verbatim = self.rhash = None
        self.xdata = self.ydata = None

        # reaction index
        self.idx = idx

        # replace separators and tabs
        srow = row.strip().replace("->", ";").replace("\t", " ")
        # replace multiple spaces
        while "  " in srow:
            srow = srow.replace("  ", " ")

        # parse line from file
        (self.reactants, self.products, self.rate) = [x.split(" + ") for x in srow.split(";")]

        # rate has always a single value after split
        self.rate = " + ".join(self.rate).strip()
        # parse reactants and products
        self.reactants = [Species(x, mass_dict) for x in self.reactants]
        self.products = [Species(x, mass_dict) for x in self.products]

        # update reaction properties (RHS, verbatim, ...)
        self.update()

    # ********************
    # update reaction properties
    def update(self):

        # prepare RHS
        reactants_idx = [x.fidx for x in self.reactants]
        self.RHS = "k(:," + str(self.idx) + ")*ngas(:," \
                   + (")*ngas(:,".join(reactants_idx)) \
                   + ")"
        self.flux = "flux(:," + str(self.idx) + ")"

        # verbatim reaction
        self.verbatim = (" + ".join([x.name for x in self.reactants]))
        self.verbatim += " -> "
        self.verbatim += (" + ".join([x.name for x in self.products]))

        # unique reaction hash
        self.rhash = ("_".join(sorted([x.name for x in self.reactants])))
        self.rhash += "__"
        self.rhash += ("_".join(sorted([x.name for x in self.products])))

        self.xdata = []
        self.ydata = []

        # if grain involved finds rate coefficient
        self.make_dust_rate()

    # *********************
    # create dust rates
    def make_dust_rate(self):
        import matplotlib.pyplot as plt

        # check if dust involved, otherwise doesn't change self.rate
        grains = [x for x in self.reactants if x.isDust]
        if not grains:
            return

        # clear plot
        plt.clf()

        # get reactants
        sp1, sp2 = self.reactants

        # use charge value to get correct sticking, only electrons
        estick = ""
        if sp1.isElectron or sp2.isElectron:
            estick = " &\n* estick(:, " + str(grains[0].charge) + ")"

        imax = 100  # number of temperature grid points
        lTgasMin = log10(1e0)  # log of min temperature, log(K)
        lTgasMax = log10(1e6)  # log of max temperature, log(K)

        # prepare the temperature space in log
        TgasList = [1e1**(i * (lTgasMax - lTgasMin) / (imax - 1)
                          + lTgasMin) for i in range(imax)]

        # loop on Tgas to compute the rate by integrating against the size distribution
        for Tgas in TgasList:
            # Zj is always a dust grain
            if sp1.isDust:
                Zj = sp1.charge
                Zi = sp2.charge
                mi = sp2.mass
            else:
                Zi = sp1.charge
                Zj = sp2.charge
                mi = sp1.mass

            # integrate the rate against the dust distribution
            # (both particle-grain and grain-grain interactions)
            krate = self.kernel_rate(Tgas, mi, Zi, Zj,
                                     is_grain_grain=(len(grains) == 2)) + 1e-40
            # store data to fit and plot
            self.xdata.append(log10(Tgas))
            self.ydata.append(log10(krate))

        # rate as fit
        self.rate = "1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_" + sp1.fidx \
                    + "_" + sp2.fidx + "(:), krate_xmin, krate_invdx)" + estick

        # save a plot to check rate quality, stored in figs/ folder
        plt.plot(self.xdata, self.ydata)
        plt.title(self.verbatim)
        plt.xlabel("log(Tgas / K)")
        plt.ylabel("log(rate / [cm3/s])")
        fname_ref = "figs/rate_" + self.rhash.replace("-", "k").replace("+", "j")
        plt.savefig(fname_ref + ".png")

        # save data to file for further use
        self.save_data_to_file(self.xdata, self.ydata, fname_ref + ".dat")

    # *************************
    # save xdata and ydata to file fname
    @staticmethod
    def save_data_to_file(x_data, y_data, fname):
        fout = open(fname, "w")
        for ii, xx in enumerate(x_data):
            fout.write(str(xx) + " " + str(y_data[ii]) + "\n")
        fout.close()

    # ****************************************
    # compute rate against dust distribution, m2 is the particle mass (if it is grain mass
    # is recomputed inside the loops on grain distribution)
    def kernel_rate(self, Tgas, mi, Zi, Zj, is_grain_grain=False):

        # get some commons variables
        from commons import amin, amax, pi43, bulkDensity, pexp

        aminLog = log10(amin)  # log of min size
        amaxLog = log10(amax)  # log of max size
        nbins = 30  # number of size bins

        # size distribution log spaced
        asize_list = [1e1**(i * (amaxLog - aminLog) / (nbins - 1) + aminLog) for i in range(nbins)]
        p1 = pexp + 1e0

        # integral of size distribution
        int_distr = (max(asize_list)**p1 - min(asize_list)**p1) / p1

        # if grain-grain solves double integral on distributions
        if is_grain_grain:
            krate2 = []
            # loop on partner size
            for asize2 in asize_list:
                mj = pi43 * bulkDensity * asize2**3
                krate = []
                # loop on grain size
                for asize in asize_list:
                    mi_a = pi43 * bulkDensity * asize ** 3
                    if mi_a < mj:
                        Zii = Zi
                        Zjj = Zj
                    else:
                        Zii = Zj
                        Zjj = Zi
                    kf = self.frea(asize, asize2, mi_a, mj, Zii, Zjj, Tgas)
                    # multiply by phi(a)
                    kf *= asize**pexp
                    krate.append(kf)
                # solve the first integral
                kf2 = np.trapz(krate, asize_list) / int_distr
                # multiply by phi(a)
                kf2 *= asize2**pexp
                krate2.append(kf2)
            # solve the second integral
            return np.trapz(krate2, asize_list) / int_distr
        # ELSE if not dust-dust solve single integral with asize2 = 0 partner
        else:
            asize2 = 0e0
            # prepare rates by integrating the rate kerneled with the dust distribution
            krate = []
            # loop on sizes
            for asize in asize_list:
                # grain mass
                mj = pi43 * bulkDensity * asize**3
                # compute rate
                kf = self.frea(asize, asize2, mi, mj, Zi, Zj, Tgas)
                # multiply the rate by dust distribution
                kf *= asize**pexp
                # store rate value to integrate on size later
                krate.append(kf)

            # integrate rate on size and normalize to distribution integral
            return np.trapz(krate, asize_list) / int_distr

    # **********************
    # grain-cation and grain-grain rate coefficient, cm3/s
    @staticmethod
    def frea(asizei, asizej, mi, mj, Zi, Zj, Tgas):
        from commons import kboltzmann, echarge2

        # inverse of reduced mass
        imass = 1e0 / mi + 1e0 / mj
        # total size
        asize = asizei + asizej
        # geometrical pre-factor, cm3/s
        ff = pi * asize**2 * sqrt(8. * kboltzmann * Tgas / pi * imass)

        # parameters from Draine+Sutin 1986
        tau = asize * kboltzmann * Tgas / echarge2
        nu = None
        if Zi * Zj > 0e0:
            nu = Zj / Zi
            theta = nu / (1e0 + 1e0 / sqrt(nu))
        elif Zi * Zj < 0e0:
            nu = Zj / Zi
            theta = 0e0
        else:
            theta = 0e0

        # check if repulsive or attractive and compute rate
        # see Draine+Sutin 1986
        if Zi * Zj > 0:
            # ff *= exp(-abs(Z1 * Z2) * echarge2 / asize / kboltzmann / Tgas)
            ff *= (1e0 + 1e0 / sqrt(4e0*tau + 3e0*nu))**2 * exp(-theta / tau)
        elif Zi * Zj < 0:
            # ff *= (1e0 + abs(Z1 * Z2) * echarge2 / asize / kboltzmann / Tgas)
            ff *= (1e0 - nu / tau) * (1e0 + sqrt(2e0 / (tau - 2e0 * nu)))
        else:
            ff *= 1e0 + sqrt(pi / 2e0 / tau)
        return ff
