module rates
contains

  !******************
  !get reaction rate coefficients
  function getRates(n, Tgasin) result(k)
    use commons
    use crays
    implicit none
    integer,parameter::zminf=-4,zmaxf=4
    real*8,intent(in)::Tgasin(ncell), n(ncell, neq)
    real*8::k(ncell,nreacts),invT(ncell),sqrTgas(ncell)
    real*8::Tgas(ncell),logTgas2(ncell),logTgas(ncell)
    real*8::estick(ncell,zminf:zmaxf),stick(zminf:zmaxf,3)
    real*8::crflux(ncell)
    integer::i

    do i=1,ncell
       Tgas(i) = max(Tgasin(i), 1d0)
    end do

    ! coefficients for electron sticking, fit of Bai 2011, Fig.6 left, D=1eV
    ! array dimensions are charge, and polynomial fit coefficient index as
    ! k(Z) = 1e1**(c(Z,1)*log10(Tgas)**2 + c(Z,2)*log10(Tgas) + c(Z,3))
    stick(-4,:) = (/-0.419532959973507d0, 0.377713778369447d0, 0.0695070269849193d0/)
    stick(-3,:) = (/-0.418191116025009d0, 0.342617719784824d0, 0.184532602608894d0/)
    stick(-2,:) = (/-0.409082882254863d0, 0.3092232796856d0, 0.203395833878889d0/)
    stick(-1,:) = (/-0.392064558243327d0, 0.267302501057781d0, 0.161341043508456d0/)
    stick(0,:) = (/-0.36848365047918d0, 0.224987701201355d0, 0.0510027977105757d0/)
    stick(1,:) = (/-0.343503880962264d0, 0.214286626949166d0, -0.172001602327555d0/)
    stick(2,:) = (/-0.333851347469237d0, 0.319512474192877d0, -0.605502652636487d0/)
    stick(3,:) = (/-0.3519217024439d0, 0.553698252568829d0, -1.17743478125107d0/)
    stick(4,:) = (/-0.284044852294462d0, 0.183121835449623d0, -0.708259746433984d0/)

    !precompute derived quantities
    invT(:) = 1d0/Tgas(:)
    sqrTgas(:) = sqrt(Tgas(:))
    logTgas(:) = log10(Tgas(:))
    logTgas2(:) = logTgas(:)**2

    !compute sticking from fit
    do i=zmin,zmax
       estick(:,i) = 1e1**(stick(i,1)*logTgas2(:) &
            + stick(i,2)*logTgas(:) &
            + stick(i,3))
    end do

    crflux(:) = n(:, idx_zeta)

    !estick(:,:) = 0.6

    !!BEGIN_RATES
! >>>>>>>>>>>>>>>>>>>>>>>>>>>
! NOTE: This block is auto-generated
! WHEN: 2020-09-01 11:53:47
! CHANGESET: b2eba3bfee45d0064a6dc77b06b4bfbf241d3ca2
! BY: nicol@Quiltras

!H2 -> Mg+ + e-
k(:,1) = crflux(:)

!Mg+ + e- -> H2
k(:,2) = 1.92e-11 / ( sqrt(Tgas/4.849e2) * (1e0 + sqrt(Tgas/4.849e2))**0.6972 * (1e0 + sqrt(Tgas/5.89e6))**1.3028 )

!g-- + g -> g- + g-
k(:,3) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_gqq_idx_g(:), krate_xmin, krate_invdx)

!g-- + g+ -> g + g-
k(:,4) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_gqq_idx_gj(:), krate_xmin, krate_invdx)

!g-- + g++ -> g + g
k(:,5) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_gqq_idx_gjj(:), krate_xmin, krate_invdx)

!g- + g+ -> g + g
k(:,6) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_gq_idx_gj(:), krate_xmin, krate_invdx)

!g- + g++ -> g + g+
k(:,7) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_gq_idx_gjj(:), krate_xmin, krate_invdx)

!g++ + g -> g+ + g+
k(:,8) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_gjj_idx_g(:), krate_xmin, krate_invdx)

!Mg+ + g-- -> H2 + g-
k(:,9) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_Mgj_idx_gqq(:), krate_xmin, krate_invdx)

!Mg+ + g- -> H2 + g
k(:,10) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_Mgj_idx_gq(:), krate_xmin, krate_invdx)

!Mg+ + g -> H2 + g+
k(:,11) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_Mgj_idx_g(:), krate_xmin, krate_invdx)

!Mg+ + g+ -> H2 + g++
k(:,12) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_Mgj_idx_gj(:), krate_xmin, krate_invdx)

!e- + g- -> g--
k(:,13) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_el_idx_gq(:), krate_xmin, krate_invdx) &
* estick(:, -1)

!e- + g -> g-
k(:,14) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_el_idx_g(:), krate_xmin, krate_invdx) &
* estick(:, 0)

!e- + g+ -> g
k(:,15) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_el_idx_gj(:), krate_xmin, krate_invdx) &
* estick(:, 1)

!e- + g++ -> g+
k(:,16) = 1d1**fit1D(logTgas(:), krate_xdata(:), krate_ydata_idx_el_idx_gjj(:), krate_xmin, krate_invdx) &
* estick(:, 2)

    !!END_RATES

  end function getRates

  !***************
  function fit1D(xp,xdata,ydata,xmin,fit_invdx) result(f)
    use commons
    implicit none
    real*8,intent(in)::xp(ncell),xdata(:),ydata(:)
    real*8,intent(in)::xmin,fit_invdx
    real*8::f(ncell)
    integer::i,j

    do j=1,ncell
       i = int((xp(j)-xmin)*fit_invdx) + 1
       f(j) = (xp(j)-xdata(i)) / (xdata(i+1)-xdata(i)) &
            * (ydata(i+1)-ydata(i)) + ydata(i)
    end do

  end function fit1D

end module rates
