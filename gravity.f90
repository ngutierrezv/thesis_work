module gravity
contains

  ! *****************************
  subroutine get_analytic_gravity(g_acc)
    use commons
    implicit none
    integer::j
    integer,parameter::nall=ncell+2*nghost
    real*8,intent(inout)::g_acc(nall)
    real*8::g_new(nall), dist(nall), centre, mcentre, eps

    ! this is only an example, you can change the potential according to your needs
    g_new(:) = 0d0

    ! eps=1d-20
    ! centre=0.5d0*ncell
    ! mcentre=1.989d33 !msun
    ! do j=1,ncell
    !  dist(nghost+j)=dx*(centre-j+1)
    ! enddo
    ! do j=0,nghost-1
    !  dist(j+1)=dist(nghost+1)+(nghost-j)*dx
    !  dist(nghost+ncell+j+1)=dist(nghost+ncell)+(j+1)*dx
    ! enddo
    ! g_new=-grav*mcentre/(dist**2+eps**2)

    g_acc(:) = g_acc(:) + g_new(:)

  end subroutine get_analytic_gravity

  ! ***************************
  subroutine get_g_fem(rho, g_acc)
    use commons
    implicit none
    integer,parameter::nall=ncell+2*nghost
    integer::info, ipiv(nall), j
    real*8,intent(out)::g_acc(nall)
    real*8,intent(in)::rho(nall)
    real*8::b_vec(nall), A(nall,nall)
    real*8::rho_mean
    real*8::dL(nall-1), dC(nall), dU(nall-1)

    ! depending on boundary conditions initialize A matrix
    ! for tridiagonal or generic solver
    if(bndry == 2) then
       A(:, :) = 0d0
       do j=1,nall
          A(j,j) = -2d0
       end do

       do j=2,nall
          A(j,j-1) = 1d0
       end do

       do j=1,nall-1
          A(j,j+1) = 1d0
       end do
    else
       ! tridiagonal matrix
       dC(:) = -2d0  ! diagonal
       dL(:) = 1d0  ! lower band
       dU(:) = 1d0  ! upper band
    end if

    rho_mean = 0d0
    ! set boundary conditions on the matrix if required by boundary method
    if(bndry == 2) then
       A(1,nall-nghost-2) = 1d0
       A(nall,nghost+3) = 1d0
       rho_mean = sum(rho(nghost+1:nghost+ncell)) / ncell
       ! if(rho_mean.ne.0.d0) b_vec=b_vec-rho_mean*pi4*(-1.d0)*grav*dx**2 !/rho*(rho-rho_mean)
    else !outflow isolated conditions
       ! WRITE WHAT YOU NEED
    endif

    b_vec(:) = (rho - rho_mean) * pi4 * grav * dx**2 !4*pi*G*rho*dx**2

    ! choose solver depending on the problem
    if(bndry == 2) then
       ! generic Ax=B
       call dgesv(nall, 1, A(:,:), nall, ipiv(:), b_vec(:), nall, info)
    else
       ! tridiagonal Ax=B
       call dgtsv(nall, 1, dL(:), dC(:), dU(:), b_vec(:), nall, info)
    end if

    if(info /= 0) then
       print *, 'ERROR: in self gravity Ax=b solution not found, dgesv info=', info
       stop
    end if

    g_acc(:) = 0d0
    g_acc(nghost+1:ncell+nghost) = - (b_vec(nghost+2:nghost+ncell+1) - b_vec(nghost:nghost+ncell-1)) / 2 / dx

  end subroutine get_g_fem


!!!NOT TESTED YET! NOT TO BE USED
#if 0
  subroutine get_g_fft(rho,g_acc,bounds)
    use commons
    use fftw
    implicit none
    integer,parameter::nall=ncell+2*nghost
    integer :: bounds,nmode,j
    real*8 :: g_acc(nall),rho(nall)
    real*8 :: fftvec(4*nall),kwave

    if(bounds.ne.-1.and.bounds.ne.0.) then
       print *,'Not implemented'
       stop
    endif


    fftvec=0.0d0 !set to 0
    fftvec(1:2*nall:2)=rho !copy the density in the array (complex number format)

    if(bounds.eq.-1) then !in the case of periodic boundaries, we copy the density in the empty region
       fftvec(2*nall+1:4*nall:2)=rho
    endif

    call fft(fftvec,2*nall,1)

    fftvec(1:2)=0.d0 !reset the constant mode

    nmode=1
    do j=3,2*nall-1,2
       kwave=nmode*acos(-1.d0) ! nmode*2*PI/L' with L'=2*L
       fftvec(j)=-fftvec(j)/kwave**2
       fftvec(j+1)=-fftvec(j+1)/kwave**2
       fftvec(4*nall+3-j)=-fftvec(4*nall+3-j)/kwave**2
       fftvec(4*nall+2-j)=-fftvec(4*nall+2-j)/kwave**2
       nmode=nmode+1
    enddo

    kwave=nmode*acos(-1.d0) !as above
    fftvec(2*nall+1)=-fftvec(2*nall+1)/kwave**2
    fftvec(2*nall+2)=-fftvec(2*nall+2)/kwave**2

    call fft(fftvec,2*nall,-1)
    g_acc=0.d0
    g_acc(nghost+1:ncell+nghost) = -(fftvec(nghost+2:nghost+ncell+1)-fftvec(nghost:nghost+ncell-1))/2/dx
    g_acc = g_acc/2/nall*4*acos(-1.d0)*6.673d-08
  end subroutine get_g_fft
#endif

end module gravity
