module nonideal
contains

  !******************************
  !get conductivity, 1/s
  function getConductivity(n,B2,Tgasin,ngrid) result(sigma)
    use commons
    implicit none
    integer,intent(in)::ngrid
    real*8,intent(in)::n(ngrid,neq),B2(ngrid),Tgasin(ngrid)
    real*8::sigma(ngrid,3),beta(ngrid,neq)
    real*8::B(ngrid),Tgas(ngrid),invBeta2(ngrid,neq)
    integer::i

    B(:) = sqrt(B2(:))

    !avoid negative temperature
    do i=1,ngrid
       Tgas(i) = max(Tgasin(i), 1d-40)
    end do

    !init sigma
    sigma(:,:) = 0d0

    !compute beta
    beta(:,:) = getBeta(n(:,:),B(:),Tgas(:),ngrid)

    !!BEGIN_NONIDEAL
! >>>>>>>>>>>>>>>>>>>>>>>>>>>
! NOTE: This block is auto-generated
! WHEN: 2020-09-01 11:53:48
! CHANGESET: b2eba3bfee45d0064a6dc77b06b4bfbf241d3ca2
! BY: nicol@Quiltras


!compute 1/(1+beta**2)
invBeta2(:,idx_Mgj) = 1d0/(1d0+beta(:,idx_Mgj)**2)
invBeta2(:,idx_el) = 1d0/(1d0+beta(:,idx_el)**2)
invBeta2(:,idx_gqq) = 1d0/(1d0+beta(:,idx_gqq)**2)
invBeta2(:,idx_gq) = 1d0/(1d0+beta(:,idx_gq)**2)
invBeta2(:,idx_gj) = 1d0/(1d0+beta(:,idx_gj)**2)
invBeta2(:,idx_gjj) = 1d0/(1d0+beta(:,idx_gjj)**2)

!sigma parallel
sigma(:,idx_parallel) = sigma(:,idx_parallel) + (1.43543997d+14)*n(:,idx_Mgj)*beta(:,idx_Mgj)
sigma(:,idx_parallel) = sigma(:,idx_parallel) + (-5.27280932d+17)*n(:,idx_el)*beta(:,idx_el)
sigma(:,idx_parallel) = sigma(:,idx_parallel) + (-1.69876900d+09)*n(:,idx_gqq)*beta(:,idx_gqq)
sigma(:,idx_parallel) = sigma(:,idx_parallel) + (-8.49384504d+08)*n(:,idx_gq)*beta(:,idx_gq)
sigma(:,idx_parallel) = sigma(:,idx_parallel) + (8.49384506d+08)*n(:,idx_gj)*beta(:,idx_gj)
sigma(:,idx_parallel) = sigma(:,idx_parallel) + (1.69876902d+09)*n(:,idx_gjj)*beta(:,idx_gjj)

!sigma pedersen
sigma(:,idx_pedersen) = sigma(:,idx_pedersen) + (1.43543997d+14)*n(:,idx_Mgj)*beta(:,idx_Mgj)*invBeta2(:,idx_Mgj)
sigma(:,idx_pedersen) = sigma(:,idx_pedersen) + (-5.27280932d+17)*n(:,idx_el)*beta(:,idx_el)*invBeta2(:,idx_el)
sigma(:,idx_pedersen) = sigma(:,idx_pedersen) + (-1.69876900d+09)*n(:,idx_gqq)*beta(:,idx_gqq)*invBeta2(:,idx_gqq)
sigma(:,idx_pedersen) = sigma(:,idx_pedersen) + (-8.49384504d+08)*n(:,idx_gq)*beta(:,idx_gq)*invBeta2(:,idx_gq)
sigma(:,idx_pedersen) = sigma(:,idx_pedersen) + (8.49384506d+08)*n(:,idx_gj)*beta(:,idx_gj)*invBeta2(:,idx_gj)
sigma(:,idx_pedersen) = sigma(:,idx_pedersen) + (1.69876902d+09)*n(:,idx_gjj)*beta(:,idx_gjj)*invBeta2(:,idx_gjj)

!sigma Hall
sigma(:,idx_hall) = sigma(:,idx_hall) + (1.43543997d+14)*n(:,idx_Mgj)*invBeta2(:,idx_Mgj)
sigma(:,idx_hall) = sigma(:,idx_hall) + (-5.27280932d+17)*n(:,idx_el)*invBeta2(:,idx_el)
sigma(:,idx_hall) = sigma(:,idx_hall) + (-1.69876900d+09)*n(:,idx_gqq)*invBeta2(:,idx_gqq)
sigma(:,idx_hall) = sigma(:,idx_hall) + (-8.49384504d+08)*n(:,idx_gq)*invBeta2(:,idx_gq)
sigma(:,idx_hall) = sigma(:,idx_hall) + (8.49384506d+08)*n(:,idx_gj)*invBeta2(:,idx_gj)
sigma(:,idx_hall) = sigma(:,idx_hall) + (1.69876902d+09)*n(:,idx_gjj)*invBeta2(:,idx_gjj)
    !!END_NONIDEAL

    !loop to scale to c/B
    do i=1,3
       sigma(:,i) = clight*sigma(:,i) / B(:)
    end do

  end function getConductivity

  !*************************
  !get resistivity, cm2/s
  function getResistivity(n, B2, Tgas, ngrid) result(eta)
    use commons
    implicit none
    integer,intent(in)::ngrid
    real*8,intent(in)::n(ngrid,neq),B2(ngrid),Tgas(ngrid)
    real*8::eta(ngrid,3),sigma(ngrid,3),iph2(ngrid)

    ! get conductivity, 1/s
    sigma(:,:) = getConductivity(n(:,:), B2(:), Tgas(:), ngrid)

    ! default values
    eta(:,:) = 0d0

    ! store inverse of pedersen**2+hall**2
    iph2(:) = 1d0 / (sigma(:, idx_pedersen)**2 + sigma(:, idx_hall)**2)

    ! ohmic resistivity
    eta(:, idx_ohmic) = 1d0 / sigma(:, idx_parallel)

    ! ambipolar resistivity
    eta(:, idx_ambipolar) = sigma(:, idx_pedersen) * iph2(:) &
         - eta(:, idx_ohmic)

    ! Hall resistivity
    eta(:, idx_hall) = sigma(:, idx_hall) * iph2(:)

    ! scale to gauss cgs units, cm2/s
    eta(:,:) = clight**2 / pi4 * eta(:,:)

  end function getResistivity

  !************************
  !get MHD beta
  function getBeta(n, B, Tgas, ngrid) result(beta)
    use commons
    implicit none
    integer,intent(in)::ngrid
    real*8,intent(in)::n(ngrid,neq), B(ngrid), Tgas(ngrid)
    real*8::beta(ngrid,neq)
    real*8::polH2, th(ngrid)
    real*8::irhoH2(ngrid), iRdust(ngrid, zmax), Re(ngrid)
    real*8::RMgj(ngrid), iTgas25(ngrid), sqrtT(ngrid)
    real*8::Birho(ngrid), acrit(ngrid, zmax)
    integer::i

    !H2 polarizability, cm3
    polH2 = 8.04d-25

    !derived quantities
    irhoH2(:) = 1d0/n(:,idx_H2)

    !compute coefficient using Pinto+Galli 2008b
    th(:) = log10(Tgas(:))
    Re(:) = 1d-9*sqrt(Tgas(:))*(0.535+0.203*th(:)-0.163*th(:)**2+0.05*th(:)**3)
    RMgj(:) = 2.210*pi*echarge*sqrt(polH2*(imass(idx_Mgj)+imass(idx_H2)))

    iTgas25(:) = Tgas(:)**(-.25)
    sqrtT(:) = sqrt(Tgas(:))
    Birho(:) = B(:) / n(:, idx_H2)

    !Draine+1983
    !Re(:) = 1d-15*sqrt(128.*kboltzmann*Tgas(:)/9./pi/emass)
    !RMgj(:) = 1.9d-9

    !!BEGIN_BETA
! >>>>>>>>>>>>>>>>>>>>>>>>>>>
! NOTE: This block is auto-generated
! WHEN: 2020-09-01 11:53:48
! CHANGESET: b2eba3bfee45d0064a6dc77b06b4bfbf241d3ca2
! BY: nicol@Quiltras

! critical size to switch from eqn.25 to A3 (Pinto+Galli 2008), cm
acrit(:, 1) = 1.71190360d-07 * iTgas25(:)

! avoid acrit < amin
do i=1,ngrid
   acrit(i, 1) = max(acrit(i, 1), 1.00000000d-07)
end do

acrit(:, 2) = 2.03580794d-07 * iTgas25(:)

! avoid acrit < amin
do i=1,ngrid
   acrit(i, 2) = max(acrit(i, 2), 1.00000000d-07)
end do


! B / rho / rate
iRdust(:, 1) = Birho(:) / (-5.17643992d-27 * (acrit(:, 1)**(-2.5) - 3.16227766d+17)  &
 + (-8.82441278d-13) * sqrtT(:) * ( 3.16227766d+02 - acrit(:, 1)**(-0.5)))
iRdust(:, 2) = Birho(:) / (-7.32059154d-27 * (acrit(:, 2)**(-2.5) - 3.16227766d+17)  &
 + (-8.82441278d-13) * sqrtT(:) * ( 3.16227766d+02 - acrit(:, 2)**(-0.5)))

beta(:, idx_Mgj) = 3.20478913d-20 * Birho(:) / RMgj(:)
beta(:, idx_el) = -5.88848779d-17 * Birho(:) / Re(:)
beta(:, idx_gqq) = -3.20437193d-20 * iRdust(:, 2)
beta(:, idx_gq) = -1.60218596d-20 * iRdust(:, 1)
beta(:, idx_gj) = 1.60218596d-20 * iRdust(:, 1)
beta(:, idx_gjj) = 3.20437193d-20 * iRdust(:, 2)
    !!END_BETA

  end function getBeta

  !**************************
  ! get resistivity from equilibrium table, cm2/s
  ! table is x,y,z = log(B/G), log(ngas/cm-3), log(T/K)
  ! table is loaded in utils.f90
  function getResistivityTable(n, B2, Tgas, ngrid) result(eta)
    use commons
    implicit none
    integer,intent(in)::ngrid
    real*8,intent(in)::n(ngrid,neq),B2(ngrid),Tgas(ngrid)
    real*8::eta(ngrid,3)
    real*8::px(ngrid),py(ngrid),pz(ngrid)
    real*8::x(ngrid),y(ngrid),z(ngrid)
    real*8::x0(ngrid),y0(ngrid),z0(ngrid)
    real*8::x1(ngrid),y1(ngrid),z1(ngrid)
    real*8::fy0(ngrid),fy1(ngrid)
    real*8::fx00(ngrid),fx10(ngrid),fx01(ngrid),fx11(ngrid)
    real*8::f000(ngrid),f010(ngrid),f100(ngrid),f110(ngrid)
    real*8::f001(ngrid),f011(ngrid),f101(ngrid),f111(ngrid)
    integer::i,idx,idy,idz

    !get B, ngas, Tgas
    x(:) = log10(sqrt(B2(:)))
    y(:) = log10(n(:,idx_rho)/pmass)
    z(:) = log10(Tgas(:))

    !loop on grid points
    do i=1,ngrid
       !grid index
       idx = int((x(i)-tab_xmin)*tab_invdx)
       idy = int((y(i)-tab_ymin)*tab_invdy)
       idz = int((z(i)-tab_zmin)*tab_invdz)

       !get left/right limits for x,y,z
       x0(i) = idx*tab_dx + tab_xmin
       x1(i) = x0(i) + tab_dx
       y0(i) = idy*tab_dy + tab_ymin
       y1(i) = y0(i) + tab_dy
       z0(i) = idz*tab_dz + tab_zmin
       z1(i) = z0(i) + tab_dz

       idx = idx + 1
       idy = idy + 1
       idz = idz + 1

       !get f at (x,y,z)
       f000(i) = tab_etaAD(idx,   idy,   idz)
       f010(i) = tab_etaAD(idx,   idy+1, idz)
       f100(i) = tab_etaAD(idx+1, idy,   idz)
       f110(i) = tab_etaAD(idx+1, idy+1, idz)
       f001(i) = tab_etaAD(idx,   idy,   idz+1)
       f011(i) = tab_etaAD(idx,   idy+1, idz+1)
       f101(i) = tab_etaAD(idx+1, idy,   idz+1)
       f111(i) = tab_etaAD(idx+1, idy+1, idz+1)
    end do

    !useful fit quantities
    px(:) = (x(:)-x0(:)) / (x1(:)-x0(:))
    py(:) = (y(:)-y0(:)) / (y1(:)-y0(:))
    pz(:) = (z(:)-z0(:)) / (z1(:)-z0(:))

    !fit on x
    fx00(:) = px(:) * (f100(:)-f000(:)) + f000(:)
    fx10(:) = px(:) * (f110(:)-f010(:)) + f010(:)
    fx01(:) = px(:) * (f101(:)-f001(:)) + f001(:)
    fx11(:) = px(:) * (f111(:)-f011(:)) + f011(:)

    !fit on y
    fy0(:) = py(:)*(fx10(:)-fx00(:)) + fx00(:)
    fy1(:) = py(:)*(fx11(:)-fx01(:)) + fx01(:)

    eta(:,:) = 0d0
    !fit on z
    eta(:,idx_ambipolar) = 1d1**(pz(:)*(fy1(:)-fy0(:)) + fy0(:))

  end function getResistivityTable

  !****************************
  subroutine create_resistivity_table()
    use commons
    use odechem
    implicit none
    real*8::n(neq-nvar), ntot, Tgas, zeta
    real*8::nvec(1, neq), B2, eta(1,3), sigma(1,3)
    real*8::B2vec(1), Tvec(1), dt, t, d2g, Bfield
    integer::i, j, k, unit_end, unit_time

    ! dust to gas mass ratio
    d2g = 1d-2
    ! cosmic rays ionization rate, 1/s
    zeta = 1d-17

    ! files where to write time evolution and final eta/sigma
    open(newunit=unit_end, file="data/resistivity_table.dat", status="replace")
    open(newunit=unit_time, file="resistivity_table_time.dat", status="replace")

    print '(99a17)', "B/G", "ntot/cm-3", "Tgas/K", "%"

    ! loop on magnetic field steps
    do i=1,tab_imax
       ! magnetic field, G
       Bfield = 1d1**((i-1)*2./(tab_imax-1) - 5.)
       ! loop on density steps
       do j=1,tab_imax
          ! density, cm-3
          ntot = 1d1**((j-1)*(6.8-3.7)/(tab_imax-1)+3.7)
          ! loop on temperature steps
          do k=1,tab_imax
             ! Tgas, K
             Tgas = 1d1**((k-1)*3.1/(tab_imax-1))+1d0

             ! default abundances
             n(:) = 1d-40
             ! convert to H2 mass density
             n(idx_H2-nvar) = ntot*mass(idx_H2)*0.5
             ! get grains density
             n(idx_g-nvar) = n(idx_H2-nvar) * d2g
             ! magetic field squared, G^2
             B2 = Bfield**2

             ! print some output
             print '(99E17.8)', Bfield, ntot, Tgas, (k+tab_imax*(j-1) + tab_imax**2*(i-1))*1e2/tab_imax**3

             ! initial time-step, s
             dt = 1d2*spy / sqrt(ntot)
             t = 0d0
             ! loop on time to have chemistry time-evolution
             do
                dt = dt * 1.1
                t = t + dt
                ! solve chemistry
                n(:) = dochem(n(:), Tgas, zeta, dt)

                ! dump time evolution to check equilibrium
                write(unit_time, '(99E17.8e3)') ntot, t/spy, &
                     n(idx_g-nvar)/mass(idx_g), n(idx_gq-nvar)/mass(idx_gq), &
                     n(idx_gj-nvar)/mass(idx_gj), n(idx_el-nvar)/mass(idx_el), &
                     n(idx_mgj-nvar)/mass(idx_mgj)
                ! exit when tmax reached (it scales roughly as sqrt(ntot))
                if(t>=1d8*spy / sqrt(ntot)) exit
             end do
             write(unit_time,*)

             ! copy to vectors, since getResistivity and getConductivity are vecotrized
             nvec(:,:) = 0d0
             nvec(1, nvar+1:neq) = n(:)
             B2vec(:) = B2
             Tvec(:) = Tgas

             ! get non-ideal eta and sigma
             eta(:,:) = getResistivity(nvec(:,:), B2vec(:), Tvec(:), 1)
             sigma(:,:) = getConductivity(nvec(:,:), B2vec(:), Tvec(:), 1)

             ! write finals state to file
             write(unit_end, '(99E17.8e3)') Bfield, ntot, Tgas, &
                  eta(1,idx_ambipolar), eta(1, idx_ohmic), eta(1, idx_hall)
          end do
          write(unit_end, *)
       end do
       write(unit_end, *)
    end do
    close(unit_time)
    close(unit_end)
  end subroutine create_resistivity_table

  !****************************
  ! compute eta and sigma at chemical equilibrium for a barotropic collapse
  ! as in Marchand+2018 paper, https://arxiv.org/pdf/1604.05613.pdf
  subroutine evaluate_eta()
    use commons
    use odechem
    implicit none
    real*8::n(neq-nvar), ntot, Tgas, zeta
    real*8::nvec(1, neq), B2, eta(1,3), sigma(1,3)
    real*8::B2vec(1), Tvec(1), dt, t, d2g
    integer::i, imax, unit_end, unit_time

    ! dust to gas mass ratio
    d2g = 1d-1
    ! number of density steps
    imax = 30

    ! files where to write time evolution and final eta/sigma
    open(newunit=unit_time, file="evaluate_eta_time.dat", status="replace")
    open(newunit=unit_end, file="evaluate_eta.dat", status="replace")

    ! loop on density steps
    do i=1,imax
       ! density, cm-3
       ntot = 1d1**((i-1)*12./(imax-1))
       n(:) = 1d-40 ! default abundances
       ! convert to H2 mass density
       n(idx_H2-nvar) = ntot*mass(idx_H2)*0.5
       ! get grains density
       n(idx_g-nvar) = n(idx_H2-nvar) * d2g
       ! temperature at ntot, K
       Tgas = 1d1 * sqrt(1d0+(ntot/1d11)**0.8) * (1d0+(ntot/1d16))**(-.3) * (1d0+(ntot/1d21))**0.56667
       ! cosmic rays ionization rate, 1/s
       zeta = 1d-17
       ! magetic field at ntot, G
       B2 = (1.43d-7)**2*ntot

       ! print some output
       print '(99E17.8)', ntot, n(idx_g-nvar)/mass(idx_g) / ntot, Tgas

       ! initial time-step, s
       dt = 1d-2*spy/ntot
       t = 0d0
       ! loop on time to have chemistry time-evolution
       do
          dt = dt * 1.1
          t = t + dt
          ! solve chemistry
          n(:) = dochem(n(:), Tgas, zeta, dt)

          ! dump time evolution
          write(unit_time, '(99E17.8e3)') ntot, t/spy, &
               n(idx_g-nvar)/mass(idx_g), n(idx_gq-nvar)/mass(idx_gq), &
               n(idx_gj-nvar)/mass(idx_gj), n(idx_el-nvar)/mass(idx_el), &
               n(idx_mgj-nvar)/mass(idx_mgj)
          ! exit when tmax reached (it scales roughly as sqrt(ntot))
          !if(t>=1d10*spy/sqrt(ntot)) exit
          if(t>=1d12*spy) exit
       end do
       write(unit_time,*)

       ! copy to vectors, since getResistivity and getConductivity are vecotrized
       nvec(:,:) = 0d0
       nvec(1, nvar+1:neq) = n(:)
       B2vec(:) = B2
       Tvec(:) = Tgas

       ! get non-ideal eta and sigma
       eta(:,:) = getResistivity(nvec(:,:), B2vec(:), Tvec(:), 1)
       sigma(:,:) = getConductivity(nvec(:,:), B2vec(:), Tvec(:), 1)

       ! write finals state to file
       write(unit_end, '(99E17.8e3)') ntot, eta(1,:), sigma(1,:), &
            n(idx_g-nvar)/mass(idx_g), n(idx_gq-nvar)/mass(idx_gq), &
            n(idx_gj-nvar)/mass(idx_gj), n(idx_el-nvar)/mass(idx_el), &
            n(idx_mgj-nvar)/mass(idx_mgj)
    end do

    close(unit_time)
    close(unit_end)
  end subroutine evaluate_eta

end module nonideal
