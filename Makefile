#executable name
exec = test
fc = ifort  # default compiler
lsolver = -llapack  # default linear algebra

#guess if you have MKL
GREP_MKL = $(shell export | grep 'mkl')
ifneq ($(GREP_MKL),)
	lsolver = -mkl
endif

#test if ifort is present
wres = $(shell which $(fc) > /dev/null; echo $$?)
ifeq "$(wres)" "0"
	fc = ifort -fpp
	switchOPT = -O3 -xHost -ipo -ip -unroll -g $(lsolver)
	switchOPT += -traceback -qopt-zmm-usage=high
	switchPRO = $(switchOPT) -pg
	switchDBG = -O0 -check all -warn all -fpe0 -u -traceback
	switchDBG += -warn nounused -init=snan,arrays $(lsolver)
	switchOMP = $(switchOPT) -qopenmp
	nowarn = -nowarn
else
	fc = gfortran -cpp
	switchOPT = -ffree-line-length-none -O3 $(lsolver)
	switchPRO = $(switchOPT) -pg -g
	switchDBG = -fbacktrace -g
	switchDBG += -ffpe-trap=zero,overflow,invalid
	switchDBG += -fbounds-check -ffree-line-length-none -O0 $(lsolver)
	nowarn = -w
endif

DEFINES= #-DPLM
#DEFINES+= -DANALYTICGRAVITY
DEFINES+= -DSELFGRAVITY
fc+= $(DEFINES)
#default switch
switch = $(switchOPT)

#objects
objs = opkda2.o
objs += opkda1.o
objs += opkdmain.o
objs += commons.o
objs += crays.o
objs += rates.o
objs += fluxes.o
objs += odechem.o
objs += nonideal.o
objs += cooling.o
objs += heating.o
objs += fftw.o
ifneq (,$(findstring GRAVITY,$(DEFINES)))
objs += gravity.o
endif
objs += ode.o
objs += utils.o


#lib = -I/usr/include/python2.7 -lpython2.7

#default target
all:	$(objs) test.o
	$(fc) $(objs) test.o -o $(exec) $(switch) $(lib)

#ifort full debug target
debug: switch = $(switchDBG)
debug: all

profile: switch = $(switchPRO)
profile: all

omp: switch = $(switchOMP)
omp: all

#clean target
clean:
	rm -f *.o *.mod *__genmod.f90 *~ $(exec)

.PHONY: clean

#rule for f90
%.o:%.f90
	$(fc) $(switch) -c $^ -o $@

#rule for f
%.o:%.f
	$(fc) $(switch) $(nowarn) -c $^ -o $@

#rule for c
%.o:%.c
	$(cc) $(cswitch) $(lib) -c $^ -o $@
