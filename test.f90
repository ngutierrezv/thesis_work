program test
  use commons
  use utils
  use ode
  use odechem
  use nonideal, only: evaluate_eta
  use cooling, only: load_cooling_table
  implicit none
  real*8::n(ncell,neq),dt,t,tend,ntmp(neq)
  real*8::ni(ncell,neq), dt_save
  integer::unit_hydro,unit_stats,isave
  character(len=30)::filename

  !Call subroutine for cooling data
  call load_cooling_table("coolZ_CIE2012NOUV.dat")

  ! precompute eta and sigma to test
  !call evaluate_eta()

  ! create equilibrium tables
  !call create_resistivity_table()

  ! ntot vs Tgas equilibrium chemistry grid
  !call chemical_grid()

  ! set slope limiter (default: 0 - MINMOD)
  slope = 1

  ! set boundary conditions (dirichlet:0, outflow:1, periodic:2)
  bndry = 2

  ! load data from input file
  n(:,:) = load_grid("input.dat")

  ! equilibrium chemistry
  !call eq_chemistry(n)

  ! store initial conditions
  ni(:,:) = n(:,:)

  filename = "output_hydro.dat"
  call file_backup(filename)

  !open stat file
  open(newunit=unit_stats,FILE='energy.txt',status="replace")

  ! open output file
  open(newunit=unit_hydro, file=filename, status="replace")

  ! dump initial conditions
  call save_variables_to_file(n(:,:), 0d0, unit_hydro)

  ! set ending time, s
  tend = 1.3e6*spy

  ! set timestep, s
  dt_save = tend / 1d1
  isave = -1

  ! init total time, s
  t = 0d0

  ! loop on timesteps
  do

     !call set_ni_turb(ni(:, :), force_sign=.true.)
     dt = dt_save

     ! check if jeans length is resolved
     if(.not.is_jeans_resolved(n(: ,:))) then
        print *, "JEANS LENGTH NOT RESOLVED!"
        exit
     end if

     ! do the job
     n(:,:) = fdlsodes(n(:,:), ni(:,:), dt)

     ! increase time
     t = t + dt

     if(int(t / dt_save) > isave) then
        print *, "saving..."
        ! store variables to file
        call save_variables_to_file(n(:,:), t, unit_hydro)

        ! write stats to file
        call save_stats(t, unit_stats)
        isave = int(t / dt_save)
     end if

     ! breaks when done
     if(t>=tend) exit
  end do

  ! close output file
  close(unit_hydro)

  ! close stat file
  close(unit_stats)

  ! say goodbye
  print *,"done!"

contains
  subroutine save_stats(t, unit_stats)
    implicit none
    real*8::EK, ET, EB
    real*8,intent(in)::t
    integer,intent(in)::unit_stats
    EK = sum(0.5d0 * (n(:,idx_rvx)**2 + n(:,idx_rvy)**2 + n(:,idx_rvz)**2) / n(:,idx_rho))
    EB = 0.5d0 * sum(Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2) / pi4
    ET = sum(n(:,idx_E)) - EK - EB

    EK = dx**3 * EK
    ET = dx**3 * ET
    EB = dx**3 * EB
    write(unit_stats,'(5ES17.10)') t, EK, ET, EB
    flush(unit_stats)

  end subroutine save_stats

end program test
