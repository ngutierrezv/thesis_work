module utils
contains

  !********************
  !load test parameters from filename, see models folder
  function load(filename) result(n)
    use commons
    use crays
    implicit none
    character(len=*),intent(in)::filename
    character(20)::var, fmti, fmte, val_char
    real*8::n(ncell,neq), p(ncell), fion(ncell)
    real*8::vx1, vx2, vy1, vy2, vmod, d2g(ncell)
    integer::unit,ios,i,j,nregions
    logical::found
    real*8,allocatable::vals(:),xpos(:)
    integer,allocatable::lcell(:),rcell(:)

    ! default values for variables
    n(:,:) = 0d0

    ! default switches
    switch_resistivity_method = "full"
    switch_crays = .true.

    print *, "reading from ", trim(filename)

    ! open file to read
    open(newunit=unit, file=trim(filename), status='old')

    ! integer values format
    fmti = "(a20,99I17)"

    !read number of regions
    read(unit,*) nregions
    print fmti, "regions:", nregions
    allocate(vals(nregions), xpos(nregions-1))
    allocate(lcell(nregions), rcell(nregions))
    !read interfaces positions (normalized to 1.0)
    read(unit,*) xpos(:)

    !set cell spacing
    lcell(1) = 1
    do i=1,size(vals)-1
       rcell(i) = ncell*xpos(i)
       lcell(i+1) = rcell(i) + 1
    end do
    rcell(size(vals)) = ncell

    print fmti,"lcell:", lcell
    print fmti,"rcell:", rcell

    fmte = "(a20,99E17.8e3)"
    do
       !read variable name
       read(unit, '(a)', iostat=ios) var

       !EOF
       if(ios/=0) exit

       !skip blanks
       if(trim(var)=="") cycle
       if(var(1:1)=="#") cycle

       ! use resistivity
       if(trim(var)=="resistivity" .or. trim(var)=="AD") then
          read(unit,*,iostat=ios) val_char
          switch_resistivity_method = trim(val_char)
          cycle
       end if

       !read values
       read(unit,*,iostat=ios) vals(:)

       !ngas is number density in cm3
       if(trim(var)=="ngas") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rho) = vals(i)*pmass*mu
             print fmte, "ngas->rho", vals(i), n(lcell(i),idx_rho)
          end do
          cycle
       end if

       !rho mass density, g/cm3
       if(trim(var)=="rho") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rho) = vals(i)
          end do
          cycle
       end if

       !use gas temperature to compute pressure
       if(trim(var)=="Tgas") then
          do i=1,nregions
             p(lcell(i):rcell(i)) = vals(i)*kboltzmann &
                  * n(lcell(i):rcell(i),idx_rho)/pmass/mu
             print fmte, "Tgas->P", vals(i), p(lcell(i))
          end do
          cycle
       end if

       !pressure from speed of sound
       if((trim(var)=="cs") .or. (trim(var)=="csound")) then
          do i=1,nregions
             p(lcell(i):rcell(i)) = vals(i)**2 &
                  * n(lcell(i):rcell(i),idx_rho) / gamma
             print fmte,"cs->P", vals(i), p(lcell(i))
          end do
          cycle
       end if

       !if pressure found, store to compute energy below
       if(trim(var)=="p") then
          do i=1,nregions
             p(lcell(i):rcell(i)) = vals(i)
          end do
          print fmte,trim(var),vals(:)
          cycle
       end if

       !momentum from vx
       if(trim(var)=="vx") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rvx) = &
                  vals(i) * n(lcell(i):rcell(i),idx_rho)
          end do
          print fmte,trim(var),vals(:)
          cycle
       end if

       !momentum from vy
       if(trim(var)=="vy") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rvy) = &
                  vals(i) * n(lcell(i):rcell(i),idx_rho)
          end do
          print fmte, trim(var), vals(:)
          cycle
       end if

       !momentum from vz
       if(trim(var)=="vz") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_rvz) = &
                  vals(i) * n(lcell(i):rcell(i),idx_rho)
          end do
          print fmte, trim(var), vals(:)
          cycle
       end if

       !ionization fraction
       if(trim(var)=="fion") then
          do i=1,nregions
             fion(lcell(i):rcell(i)) = vals(i)
          end do
          print fmte, trim(var), vals(:)
          cycle
       end if

       !ionization fraction
       if(trim(var)=="crays") then
          do i=1,nregions
             n(lcell(i):rcell(i),idx_zeta) = vals(i)
          end do
          switch_crays = .false.
          print fmte, trim(var), vals(:)
          cycle
       end if

       !dust/gas mass ratio
       if(trim(var)=="d2g") then
          do i=1,nregions
             d2g(lcell(i):rcell(i)) = vals(i)
          end do
          print fmte, trim(var), vals(:)
          cycle
       end if

       !search for variable name
       found = .false.
       !loop on var names to find match
       do i=1,neq
          if(var==varNames(i)) then
             print fmte,trim(var),vals(:)
             found = .true.
             !set left/right conditions
             do j=1,nregions
                n(lcell(j):rcell(j),i) = vals(j)
             end do
          end if
       end do
       !rise error if variable not found
       if(.not.(found)) then
          print *,"ERROR: variable not known ",trim(var)
          print *,"variables are"
          print *,varNames(:)
          stop
       end if
    end do
    close(unit)

    ! TODO: this is hardcoded should be automatized
    n(:,idx_H2) = n(:,idx_rho) / (1d0 + d2g(:) + fion(:) * (emass + mass(idx_Mgj)) / mass(idx_H2))
    n(:,idx_el) = fion(:) * n(:,idx_H2) * emass / mass(idx_H2)
    n(:,idx_Mgj) = fion(:) * n(:,idx_H2) * mass(idx_Mgj) / mass(idx_H2)
    n(:,idx_g) = n(:, idx_H2) * d2g(:)

    !compute energy
    n(:,idx_E) = 0.5*(n(:,idx_rvx)**2 + n(:,idx_rvy)**2 &
         + n(:,idx_rvz)**2) / n(:,idx_rho) &
         + 0.5*(Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2) / pi4  &
         + p(:) / (gamma-1d0)

    ! cosmic rays
    if(switch_crays) then
       n(:, idx_zeta) = get_crays(n(:,:))
    end if

    fmte = "(a20,99E17.8e3)"
    fmti = "(a20,99I10)"
    do i=1,nregions
       print *,"--------"
       print fmti,"region", i
       print fmte,"E/erg", n(lcell(i),idx_E)
       print fmte,"Tgas/K", p(lcell(i))*pmass*mu/n(lcell(i),idx_rho)/kboltzmann
       print fmte,"vx/(km/s)", n(lcell(i),idx_rvx)/n(lcell(i),idx_rho)/1d5
       print fmte,"vy/(km/s)", n(lcell(i),idx_rvy)/n(lcell(i),idx_rho)/1d5
       print fmte,"vz/(km/s)", n(lcell(i),idx_rvz)/n(lcell(i),idx_rho)/1d5
       vmod = sqrt(n(lcell(i),idx_rvx)**2 + n(lcell(i),idx_rvy)**2 + n(lcell(i),idx_rvz)**2) / n(lcell(i),idx_rho)
       print fmte,"vmod/(km/s)", vmod / 1d5
       print fmte,"cs/(km/s)", sqrt(p(lcell(i))/n(lcell(i),idx_rho)*gamma) / 1d5
       print fmte,"Mach", vmod / sqrt(p(lcell(i))/n(lcell(i),idx_rho)*gamma)
       print fmte,"vA/(km/s)", sqrt(Bx**2 + n(lcell(i),idx_By)**2 + n(lcell(i),idx_Bz)**2) / sqrt(4d0*pi*n(lcell(i), idx_rho)) / 1d5
       print fmte,"rho/(g/cm3)", n(lcell(i),idx_rho)
       print fmte,"sum rho_i/(g/cm3)", n(lcell(i), idx_H2) &
            + n(lcell(i) ,idx_Mgj) &
            + n(lcell(i), idx_el) &
            + n(lcell(i), idx_g)
       print fmte,"rho_H2/(g/cm3)", n(lcell(i),idx_H2)
       print fmte,"rho_Mg+/(g/cm3)", n(lcell(i),idx_Mgj)
       print fmte,"rho_e-/(g/cm3)", n(lcell(i),idx_el)
       print fmte,"rho_g/(g/cm3)", n(lcell(i),idx_g)
       print fmte,"n_H2/(cm-3)", n(lcell(i),idx_H2) / mass(idx_H2)
       print fmte,"n_Mg+/(cm-3)", n(lcell(i),idx_Mgj) / mass(idx_Mgj)
       print fmte,"n_e-/(cm-3)", n(lcell(i),idx_el) / mass(idx_el)
       print fmte,"n_g/(cm-3)", n(lcell(i),idx_g) / mass(idx_g)
       print fmte,"zeta CR/(s-1)", n(lcell(i), idx_zeta)
    end do

    print *, ""
    do i=1,nregions
       print *, "-------angles------- region", i
       vx1 = n(lcell(i), idx_rvx) / n(lcell(i), idx_rho)
       vy1 = n(lcell(i), idx_rvy) / n(lcell(i), idx_rho)
       print fmte, "vy/vx", atan2(vx1, vy1), atan2(vy1, vx1) * 180. / pi
       print fmte, "By/Bx", atan2(n(lcell(i), idx_By), Bx), atan2(n(lcell(i), idx_By), Bx) * 180. / pi
    end do

    !load table resistivity
    ! log10(B), log10(ngas), log10(tgas), ohmic, ambipolar, hall
    call loadResistivityTab()

    print *,"done reading!"

  end function load

  ! *******************
  ! load data from a grid of ncell points
  ! ngas/cm-3, vx/cm/s, vy/cm/s, vz/cm/s, By/G, Bz/G, Tgas/K, zeta/s-1, d2g, fion
  function load_grid(filename) result(n)
    use commons
    use crays
    implicit none
    character(len=*),intent(in)::filename
    character(20)::fmte
    real*8::n(ncell,neq), ngas, vx, vy, vz, By, Bz, zeta
    real*8::d2g(ncell), fion(ncell), p(ncell), Tgas(ncell), vA(ncell), vmod(ncell)
    real*8::drho(ncell)
    integer::unit, i, ios

    ! default switches
    switch_resistivity_method = "full" !"full" !"none"
    switch_crays = .false.  ! when false uses value found in the loaded file

    ! default values for variables
    n(:,:) = 0d0

    print *, "reading grid from ", trim(filename)

    ! open file to read
    open(newunit=unit, file=trim(filename), status='old')
    do i=1,ncell
       read(unit, *, iostat=ios) ngas, vx, vy, vz, By, Bz, Tgas(i), zeta, d2g(i), fion(i)
       n(i, idx_rho) = ngas * pmass * mu
       n(i, idx_rvx) = vx * n(i, idx_rho)
       n(i, idx_rvy) = vy * n(i, idx_rho)
       n(i, idx_rvz) = vz * n(i, idx_rho)
       n(i, idx_By) = By
       n(i, idx_Bz) = Bz
       p(i) = Tgas(i) * kboltzmann * n(i, idx_rho) / pmass / mu
       n(i, idx_zeta) = zeta
    end do
    close(unit)

    ! TODO: this is hardcoded should be automatized
    n(:,idx_H2) = n(:,idx_rho) / (1d0 + d2g(:) + fion(:) * (emass + mass(idx_Mgj)) / mass(idx_H2))
    n(:,idx_el) = fion(:) * n(:,idx_H2) * emass / mass(idx_H2)
    n(:,idx_Mgj) = fion(:) * n(:,idx_H2) * mass(idx_Mgj) / mass(idx_H2)
    n(:,idx_g) = n(:, idx_H2) * d2g(:)

    !compute energy
    n(:,idx_E) = 0.5 * (n(:, idx_rvx)**2 + n(:, idx_rvy)**2 &
         + n(:,idx_rvz)**2) / n(:,idx_rho) &
         + 0.5 * (Bx**2 + n(:, idx_By)**2 + n(:, idx_Bz)**2) / pi4  &
         + p(:) / (gamma-1d0)

    fmte = "(a20,99E17.8e3)"

    print *, "min / max"
    print fmte,"E/erg", minval(n(:,idx_E)), maxval(n(:,idx_E))
    print fmte,"Tgas/K", minval(Tgas(:)), maxval(Tgas(:))
    print fmte,"vx/(km/s)", minval(n(:,idx_rvx) / n(:,idx_rho)) / 1d5, maxval(n(:,idx_rvx) / n(:,idx_rho)) / 1d5
    print fmte,"vy/(km/s)", minval(n(:,idx_rvy) / n(:,idx_rho)) / 1d5, maxval(n(:,idx_rvy) / n(:,idx_rho)) / 1d5
    print fmte,"vz/(km/s)", minval(n(:,idx_rvz) / n(:,idx_rho)) / 1d5, maxval(n(:,idx_rvz) / n(:,idx_rho)) / 1d5
    vmod = sqrt(n(:,idx_rvx)**2 + n(:,idx_rvy)**2 + n(:,idx_rvz)**2) / n(:,idx_rho)
    print fmte,"vmod/(km/s)", minval(vmod) / 1d5, maxval(vmod) / 1d5
    print fmte,"Mach", minval(vmod / sqrt(p(:) / n(:,idx_rho) * gamma)), maxval(vmod / sqrt(p(:) / n(:,idx_rho) * gamma))
    vA(:) = sqrt(Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2) / sqrt(4d0*pi*n(:, idx_rho)) / 1d5
    print fmte,"vA/(km/s)", minval(vA), maxval(vA)
    print fmte,"Bx/G", Bx, Bx
    print fmte,"By/G", minval(n(:, idx_By)), maxval(n(:, idx_By))
    print fmte,"Bz/G", minval(n(:, idx_Bz)), maxval(n(:, idx_Bz))
    print fmte,"rho/(g/cm3)", minval(n(:, idx_rho)), maxval(n(:, idx_rho))
    print fmte,"zeta CR/(s-1)", minval(n(:, idx_zeta)), maxval(n(:, idx_zeta))

    print fmte, "H2/rho", minval(n(:,idx_H2)/n(:,idx_rho)), maxval(n(:,idx_H2)/n(:,idx_rho))
    print fmte, "e-/rho", minval(n(:,idx_el)/n(:,idx_rho)), maxval(n(:,idx_el)/n(:,idx_rho))
    print fmte, "Mg+/rho", minval(n(:,idx_Mgj)/n(:,idx_rho)), maxval(n(:,idx_Mgj)/n(:,idx_rho))
    print fmte, "g/rho", minval(n(:,idx_g)/n(:,idx_rho)), maxval(n(:,idx_g)/n(:,idx_rho))

    drho(:) = abs(sum(n(:, nvar+1:neq), dim=2) - n(:, idx_rho)) / n(:, idx_rho)
    print fmte, "rho diff", minval(drho), maxval(drho)

  end function load_grid

  ! ******************
  subroutine eq_chemistry(n)
    use commons
    use odechem
    implicit none
    real*8,intent(inout)::n(ncell, neq)
    real*8::dt_chem, irho, v2, Tgas, B2
    integer::i

    print *, "solve equilibrium chemistry"
    ! solve chemistry
    do i=1,ncell
       dt_chem = spy * 1d4
       irho = 1d0 / n(i, idx_rho)
       v2 = (n(i, idx_rvx)**2 + n(i, idx_rvy)**2 + n(i, idx_rvz)**2) * irho**2
       B2 = Bx**2 + n(i, idx_By)**2 + n(i, idx_Bz)**2
       Tgas = (gamma - 1d0) * (n(i, idx_E) - 0.5d0 * n(i, idx_rho) * v2 &
            - 0.5d0 * B2 / pi4) / kboltzmann * irho * mu * pmass
       n(i, nvar+1:neq) = dochem(n(i, nvar+1:neq), Tgas, n(i, idx_zeta), dt_chem)
    end do

    print *, "done"

  end subroutine eq_chemistry

  ! ******************
  subroutine load_turbulence_pool(filename)
    use commons
    implicit none
    character(len=*),intent(in)::filename
    integer::unit, i, ios, tsize

    ! open file to read
    open(newunit=unit, file=trim(filename), status='old')
    read(unit, *) tsize
    if(tsize/=turb_pool_size) then
       print *, "ERROR: turbulence pool size mismatching!"
       print *, "expecting", turb_pool_size, "found", tsize
       stop
    end if

    do i=1,turb_pool_size
       read(unit, *) turb_pool(i)
    end do

    close(unit)

    print *, filename//" loaded!"
    print *, "vmin, vmax", minval(turb_pool), maxval(turb_pool)

  end subroutine load_turbulence_pool

  ! ******************
  subroutine set_ni_turb(ni, force_sign)
    use commons
    implicit none
    real*8,intent(inout)::ni(ncell, neq)
    logical,intent(in),optional::force_sign
    real*8::p(ncell)
    integer::idx, i, rv(3)

    rv(:) = (/idx_rvx, idx_rvy, idx_rvz/)

    do i=1,3
       idx = int(rand() * (turb_pool_size - 1)) + 1
       ni(1, rv(i)) = ni(1, idx_rho) * turb_pool(idx)

       idx = int(rand() * (turb_pool_size - 1)) + 1
       ni(ncell, rv(i)) = ni(ncell, idx_rho) * turb_pool(idx)
    end do

    if(present(force_sign)) then
       if(force_sign) then
          ni(1, idx_rvx) = abs(ni(1, idx_rvx))
          ni(ncell, idx_rvx) = - abs(ni(ncell, idx_rvx))
       end if
    end if

    ! FIXME TGAS=15 K
    p(:) = 1.5d1 * kboltzmann * ni(:, idx_rho) / pmass / mu

    ni(1, idx_E) = 0.5 * (ni(1, idx_rvx)**2 + ni(1, idx_rvy)**2 &
         + ni(1, idx_rvz)**2) / ni(1, idx_rho) &
         + 0.5 * (Bx**2 + ni(1, idx_By)**2 + ni(1, idx_Bz)**2) / pi4  &
         + p(1) / (gamma - 1d0)

    ni(ncell, idx_E) = 0.5 * (ni(ncell, idx_rvx)**2 + ni(ncell, idx_rvy)**2 &
         + ni(ncell, idx_rvz)**2) / ni(ncell, idx_rho) &
         + 0.5 * (Bx**2 + ni(ncell, idx_By)**2 + ni(ncell, idx_Bz)**2) / pi4  &
         + p(ncell) / (gamma - 1d0)

  end subroutine set_ni_turb


  !********************
  function is_jeans_resolved(n) result(ok)
    use commons
    implicit none
    real*8,intent(in)::n(ncell, neq)
    real*8::Tgas(ncell), lj2(ncell)
    logical::ok

    Tgas(:) = getTemperature(n(:, :))

    lj2(:) = 15d0 * kboltzmann * Tgas(:) &
         / 4d0 / pi / grav / pmass / mu / n(:, idx_rho)

    !print '(a4,99E17.8e3)', "lj", sqrt(minval(lj2)), dx

    ok = minval(lj2) >= 16d0 * dx**2

  end function is_jeans_resolved

  !********************
  subroutine file_backup(filename)
    implicit none
    character(len=*),intent(in)::filename
    logical::exists

    inquire(file=trim(filename), exist=exists)
    if(exists) then
       call system("cp "//trim(filename)//" "//trim(filename)//".bak")
       print *, trim(filename)//" backup as "//trim(filename)//".bak"
    else
       print *, "WARNING: no backup for "//trim(filename)//" since not present"
    end if

  end subroutine file_backup

  !********************
  subroutine save_variables_to_file(n, time, unit)
    use commons
    use cooling
    use heating
    use nonideal
    use fluxes
    use crays
    use ode
#ifdef SELFGRAVITY
    use gravity
#endif
    implicit none
    integer,parameter::nall=order*(ncell+2*nghost)
    integer,intent(in)::unit
    real*8,intent(in)::time, n(ncell,neq)
    real*8::ntmp(neq), ni(ncell,neq), p(ncell), xpos, Tgas(ncell)
    real*8::beta(ncell,neq), eta(ncell, 3), B2(ncell), v2(ncell)
    real*8::cool(ncell), heat(ncell), ctime(ncell), zeta(ncell)
    real*8::tmpr(nreacts), flux(ncell, nreacts), eta_tab(ncell, 3)
    real*8::ng(nall, neq), g_acc(nall), g_acc_norm(ncell)
    real*8::vx(ncell),vy(ncell),vz(ncell)
    integer::i, j, unit_init, unit_hydro, unit_beta, unit_mhd_vars

    ! temporary variables
    B2(:) = Bx**2 + n(:, idx_By)**2 + n(:, idx_Bz)**2
    vx(:) = n(:,idx_rvx) / n(:, idx_rho)
    vy(:) = n(:,idx_rvy) / n(:, idx_rho)
    vz(:) = n(:,idx_rvz) / n(:, idx_rho)
    v2(:) = vx(:)**2 + vy(:)**2 + vz(:)**2
    Tgas(:) = getTemperature(n(:,:))
    eta(:, :) = getResistivity(n(:,:), B2(:), Tgas(:), ncell)
    eta_tab(:, :) = 0d0 !getResistivityTable(n(:,:), B2(:), Tgas(:), ncell)
    cool(:) = fcool(n(:,:), Tgas(:))
    heat(:) = fheat(n(:,:), Tgas(:))
    ctime(:) = n(:, idx_E) / (cool(:) - heat(:))
    beta(:,:) = getBetaMHD(n(:,:))
    flux(:,:) = getFlux(n(:,:), Tgas(:))
    zeta(:) = get_crays(n(:,:))

    ! default acceleration value
    g_acc_norm(:) = 0d0

#ifdef SELFGRAVITY
    ni(:, :) = n(:, :)
    ng(:, :) = set_boundaries(n(:, :), ni(:, :), time)
    call get_g_fem(ng(:, idx_rho), g_acc(:))
    g_acc_norm(:) = g_acc(nghost+1:nghost+ncell)
#endif

    ! loop on cells to write
    do i=1,ncell
       ! temporary variables
       ntmp(:) = n(i,:)
       tmpr(:) = flux(i,:)
       write(unit, '(999E17.8e3)') i*dx, time, Tgas(i), ntmp(:), zeta(i), &
            vx(i), vy(i), vz(i), sqrt(B2(i)), &
            eta(i, idx_ambipolar), eta(i,idx_ohmic), eta(i, idx_hall), &
            eta_tab(i, idx_ambipolar), eta_tab(i,idx_ohmic), eta_tab(i, idx_hall), &
            ntmp(nvar+1:neq) / ntmp(idx_rho), &
            cool(i), heat(i), ctime(i)/spy, beta(i, idx_Mgj) * n(i, idx_Mgj), &
            beta(i, idx_el) * n(i, idx_el), beta(i, idx_gqq) * n(i, idx_gqq), &
            beta(i, idx_gq) * n(i, idx_gq), beta(i, idx_gj) * n(i, idx_gj), &
            beta(i, idx_gjj) * n(i, idx_gjj), tmpr(:), g_acc_norm(i), g_acc_norm(i) * n(i, idx_rho), &
            0.5 * n(i, idx_rho) * v2(i), 0.5 * B2(i) / pi4
    end do
    write(unit, *)

  end subroutine save_variables_to_file


  !******************
  subroutine save_ode(n, time, unit)
    use commons
    use ode
    implicit none
    integer,intent(in)::unit
    real*8,intent(in)::time, n(ncell,neq)
    real*8::dn(ncell,neq), ntmp(neq)
    integer::i

    dn(:,:) = fhll(n, n, time)
    ! loop on cells to write
    do i=1,ncell
       ! temporary variables
       ntmp(:) = dn(i,:)
       write(unit, '(99E17.8e3)') i*dx, time, ntmp(:)
    end do
    write(unit, *)

  end subroutine save_ode

  !******************
  ! compute and append MHD variables to unit file using
  ! xvar as custom variable (e.g. time)
  subroutine dumpMHDvars(n, xvar, unit)
    use commons
    use nonideal
    implicit none
    integer,parameter::nall=ncell+2*nghost
    integer,intent(in)::unit
    real*8,intent(in)::n(ncell,neq),xvar
    real*8::ap,am,dn(ncell,neq)
    real*8::ng(nall,neq),irho(nall)
    real*8::dBy(nall),dBz(nall)
    real*8::By(nall),Bz(nall)
    real*8::vx(nall),vy(nall),vz(nall)
    real*8::v2(nall),B2(nall),Tgas(nall)
    real*8::Flx(nall),Fly(nall),Flz(nall)
    real*8::EADy(nall),EADz(nall),p(nall),pstar(nall)
    real*8::EADBx(nall),adf(nall),eta(nall,3)
    real*8::p_min(nall)
    integer::i,j

    !copy non-ghost cell into ghost-exted structure
    ng(nghost+1:nghost+ncell,:) = n(:,:)

    !set outflow boundary conditions
    do j=1,nghost
       ng(j,:) = ng(nghost+1,:)
       ng(nghost+ncell+j,:) = ng(nghost+ncell,:)
    end do

    !derived quantities from U
    irho(:) = 1d0/ng(:,idx_rho)
    vx(:) = ng(:,idx_rvx)*irho(:)
    vy(:) = ng(:,idx_rvy)*irho(:)
    vz(:) = ng(:,idx_rvz)*irho(:)
    v2(:) = vx(:)**2 + vy(:)**2 + vz(:)**2
    By(:) = ng(:,idx_By)
    Bz(:) = ng(:,idx_Bz)
    B2(:) = (Bx**2 + ng(:,idx_By)**2 + ng(:,idx_Bz)**2)
    !Tgas(:) = (gamma-1d0) * (ng(:,idx_E) - 0.5d0*ng(:,idx_rho)*v2(:) &
    !     - 0.5d0*B2(:)) / kboltzmann
     Tgas(:) = max( (gamma-1d0) * (ng(:,idx_E) - 0.5d0*ng(:,idx_rho)*v2(:) &
         - 0.5d0*B2(:)) / kboltzmann * irho(:) * mu * pmass, 1d1)

    !dBy/dx and dBz/dx, i-centered
    do j=2,nall-1
       dBy(j) = (By(j+1)-By(j-1))*invdx*.5
       dBz(j) = (Bz(j+1)-Bz(j-1))*invdx*.5
    end do
    dBy(1) = 0d0
    dBy(nall) = 0d0
    dBz(1) = 0d0
    dBz(nall) = 0d0

    !Lorentz force, (curl B) x B
    Flx(:) = -dBy(:)*By(:)-dBz(:)*Bz(:)
    Fly(:) = dBy(:)*Bx
    Flz(:) = dBz(:)*Bx

    !EAD: ambipolar EMF, (F x B)/rho_n/rho_i/gammaAD
    !adf(:) = irho(:)**2*igAD/1d-2
    eta(:,:) = getResistivity(ng(:,:),B2(:),Tgas(:),nall)
    adf(:) = eta(:,idx_ambipolar)/B2(:)
    EADy(:) = (Flz(:)*Bx-Flx(:)*Bz(:)) * adf(:)
    EADz(:) = (Flx(:)*By(:)-Fly(:)*Bx) * adf(:)

    !EAD x B, x-component only
    EADBx(:) = EADy(:)*Bz(:)-EADz(:)*By(:)

    !pressure
    p_min(:) = ng(:,idx_rho)*kboltzmann*1d1 / mu / pmass
    p(:) = max( (gamma-1d0)*(ng(:,idx_E) - 0.5*ng(:,idx_rho)*v2(:) - 0.5*B2(:) / pi4) , p_min(:) )
    !p(:) = (gamma-1d0)*(ng(:,idx_E) - 0.5*ng(:,idx_rho)*v2(:) - 0.5*B2(:)/pi4)
    pstar(:) = p(:) + 0.5*B2(:)/pi4

    !open(newunit=unit,file="dataMHD.out",status='replace')
    !1:icell, 2:time, 3:adf, 4:eta, 5:B2, 6:Flz, 7:Fly, 8:EADy
    !9:EADz, 10:EADBx, 11:dBy, 12:dBz, 13:Tgas, 14:FE, 15:FBy, 16:FBz
    !17:dBy*eta, 18:1/rho/75
    do i=1,nall
       write(unit,'(I5,99E17.8e3)') i-nghost, xvar, adf(i), &
            eta(i,idx_ambipolar), &
            B2(i), Flz(i), Fly(i), &
            EADy(i), EADz(i), EADBx(i), dBy(i), dBz(i), Tgas(i), &
            (ng(i,idx_E) + pstar(i)) * vx(i) &
            - Bx*(Bx*vx(i) + ng(i,idx_By)*vy(i) + ng(i,idx_Bz)*vz(i)), &
            ng(i,idx_By)*vx(i) - Bx*vy(i), &
            ng(i,idx_Bz)*vx(i) - Bx*vz(i), &
            dBy(i)*eta(i,idx_ambipolar), irho(i)/75.
    end do
    write(unit,*)
    !close(unit)

  end subroutine dumpMHDvars

  !****************
  ! dump a table to file changing density and ionization fraction
  ! 1:Bfield, 2:ntot, 3:Tgas, 4:f_ion, 5:eta(AD)/B**2, 6:eta(AD)
  subroutine nonidealExplore()
    use commons
    use nonideal
    implicit none
    integer,parameter::imax=30,jmax=30,ngrid=1
    integer::i,j,unit
    real*8::n(ngrid,neq),ngas(neq),Tgas(ngrid),B2(ngrid)
    real*8::ntot,fion,eta(ngrid,3),Bfield
    real*8::ngasMin,ngasMax,bMin,bMax,TgasMin,TgasMax,fionMin,fionMax

    ngasMin = log10(1d0)
    ngasMax = log10(1d10)
    bMin = log10(1d-8)
    bMax = log10(1d0)
    TgasMin = log10(1d1)
    TgasMax = log10(1d4)
    fionMin = log10(1d-6)
    fionMax = log10(9.99999d-1)

    fion = 1d-5
    Bfield = 1d-5

    Tgas(:) = 5d1
    n(:,:) = 0d0

    open(newunit=unit,file="explore.dat",status="replace")
    do j=1,jmax
       !Bfield = 1d1**((j-1)*(bMax-bMin)/(jmax-1)+bMin)
       !Tgas(:) = 1d1**((j-1)*(TgasMax-TgasMin)/(jmax-1)+TgasMin)
       fion = 1d1**((j-1)*(fionMax-fionMin)/(jmax-1)+fionMin)
       B2(:) = Bfield**2
       do i=1,imax
          ntot = 1d1**((i-1)*(ngasMax-ngasMin)/(imax-1)+ngasMin)
          n(1,idx_H2) = mass(idx_H2)*ntot*(1d0-fion)
          n(1,idx_el) = mass(idx_el)*ntot*fion
          n(1,idx_Mgj) = mass(idx_Mgj)*ntot*fion
          eta(:,:) = getResistivity(n(:,:), B2(:), Tgas(:), ngrid)

          write(unit,'(99E17.8e3)') Bfield, ntot, Tgas(1), fion, &
               eta(1, idx_ambipolar) / Bfield**2, eta(1, idx_ambipolar)
       end do
       write(unit,*)
    end do
    close(unit)

    print *,"nonideal explore data saved!"

  end subroutine nonidealExplore

  !******************
  function getTemperature(n) result(Tgas)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq)
    real*8::irho(ncell),vx(ncell),vy(ncell),vz(ncell)
    real*8::v2(ncell),B2(ncell),Bz(ncell),By(ncell)
    real*8::Tgas(ncell)
    integer::i

    !derived quantities from U
    irho(:) = 1d0 / n(:,idx_rho)
    vx(:) = n(:,idx_rvx)*irho(:)
    vy(:) = n(:,idx_rvy)*irho(:)
    vz(:) = n(:,idx_rvz)*irho(:)
    v2(:) = vx(:)**2 + vy(:)**2 + vz(:)**2
    By(:) = n(:,idx_By)
    Bz(:) = n(:,idx_Bz)
    B2(:) = (Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2)
    Tgas(:) = max( (gamma-1d0) * (n(:,idx_E) - 0.5d0 * n(:,idx_rho) * v2(:) &
         - 0.5d0 * B2(:) / pi4) / kboltzmann * irho(:) * mu * pmass , 1d1 )
    
  end function getTemperature

  !*********************
  !print fluxes using n(:,:)
  ! np(:) is an array of size nps containing selected cells for output
  ! example call with 2 sample points (ncell/4 and ncell/2) is
  ! call printFlux(n(:,:),(/ncell/4,ncell/2/),2)
  subroutine printFlux(n,np,nps)
    use commons
    use fluxes
    implicit none
    integer,intent(in)::nps,np(nps)
    real*8,intent(in)::n(ncell,neq)
    real*8::flux(ncell,nreacts),Tgas(ncell)
    integer::i,j,idx(nreacts)
    character(len=reactionNameLen)::names(nreacts)

    !get temperature
    Tgas(:) = getTemperature(n(:,:))

    !get fluxes
    flux(:,:) = getFlux(n(:,:),Tgas(:))

    !get reaction names
    names(:) = getReactionNames()

    !loop on points
    do j=1,nps
       print *,"*********"
       print *,"icell:",np(j)
       print *,"Tgas/K",Tgas(np(j))
       !sort fluxes
       idx(:) = sortf(flux(np(j),:),nreacts)
       !loop on reactions to print fluxes
       do i=1,nreacts
          print *,i,flux(np(j),idx(i)),names(idx(i))
       end do
    end do

  end subroutine printFlux

  !********************
  !not-so-efficient bubble sorting, returns array indexes
  function sortf(fin,nf) result(idx)
    use commons
    implicit none
    integer,intent(in)::nf
    real*8,intent(in)::fin(nf)
    real*8::rtmp,f(nf)
    integer::idx(nf),i,itmp
    logical::swap

    !local copy
    f(:) = fin(:)

    !init idx array
    do i=1,nf
       idx(i) = i
    end do

    !loop until sorted
    do
       swap = .false.
       do i=2,nf
          !check if sorted
          if(f(i-1)<f(i)) then
             !swap f if not sorted
             rtmp = f(i)
             f(i) = f(i-1)
             f(i-1) = rtmp
             !swap idx
             itmp = idx(i)
             idx(i) = idx(i-1)
             idx(i-1) = itmp
             swap = .true.
          end if
       end do
       !break when nothing more to swap
       if(.not.swap) exit
    end do

  end function sortf

  !******************
  function getBetaMHD(n) result(beta)
    use commons
    use nonideal
    implicit none
    real*8,intent(in)::n(ncell,neq)
    real*8::beta(ncell,neq),B2(ncell),B(ncell),Tgas(ncell)

    B2(:) = (Bx**2 + n(:,idx_By)**2 + n(:,idx_Bz)**2)
    B(:) = sqrt(B2(:))
    Tgas(:) = getTemperature(n(:,:))

    beta(:,:) = getBeta(n(:,:),B(:),Tgas(:),ncell)

  end function getBetaMHD

  !**************************
  ! load resistivity from equilibrium table, cm2/s
  ! table is x,y,z = B/G, ngas/cm-3, T/K, etaAD/cm2*s-1
  ! the code applies log
  ! table is used in nonideal.f90
  subroutine loadResistivityTab()
    use commons
    implicit none
    integer::ios,unit,ii,idx,idy,idz
    real*8::rout(6),xdata(tab_imax),ydata(tab_imax),zdata(tab_imax)

    ii = 0

    !open and check if table exists
    open(newunit=unit,file="data/resistivity_table.dat",status="old",iostat=ios)
    if(ios/=0) then
       print *,"ERROR: problem loading resistivity table!"
       stop
    end if

    !loop on file to read
    do
       read(unit,*,iostat=ios) rout(:)
       if(ios<0) exit
       if(ios>0) cycle
       idz = mod(ii,tab_imax)+1
       idy = mod(int(ii/tab_imax),tab_imax)+1
       idx = int(ii/tab_imax**2)+1

       !store AD and log10 of x,y,z data
       tab_etaAD(idx, idy, idz) = log10(rout(4))
       xdata(idx) = log10(rout(1))
       ydata(idy) = log10(rout(2))
       zdata(idz) = log10(rout(3))
       ii = ii + 1
    end do
    close(unit)

    !check if read is OK
    if(minval(xdata)==maxval(xdata)) then
       print *,"ERROR: problem reading table data!"
       stop
    end if

    !get data minimum
    tab_xmin = minval(xdata)
    tab_ymin = minval(ydata)
    tab_zmin = minval(zdata)
    !get data steps
    tab_invdx = (tab_imax-1) / (maxval(xdata) - tab_xmin)
    tab_invdy = (tab_imax-1) / (maxval(ydata) - tab_ymin)
    tab_invdz = (tab_imax-1) / (maxval(zdata) - tab_zmin)
    tab_dx = 1d0 / tab_invdx
    tab_dy = 1d0 / tab_invdy
    tab_dz = 1d0 / tab_invdz

    !print some ouput
    print *, "table range:"
    print *, "B", minval(xdata), maxval(xdata)
    print *, "n", minval(ydata), maxval(ydata)
    print *, "T", minval(zdata), maxval(zdata)

  end subroutine loadResistivityTab


end module utils
