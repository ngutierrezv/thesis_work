module ode
contains
  subroutine fex(neqa,tt,nin,dnout)
    use commons
    implicit none
    integer::neqa,i
    real*8::nin(neqa),tt,dnout(neqa)
    real*8::n(ncell,neq),dn(ncell,neq)

    !roll variables
    do i=1,neq
       n(:,i) = nin(ncell*(i-1)+1:ncell*i)
    end do

    !use HLL for spatial derivative
    dn(:,:) = fhll(n(:,:), ninit(:,:), tt)

    !unroll differntials
    do i=1,neq
       dnout(ncell*(i-1)+1:ncell*i) = dn(:,i)
    end do
  end subroutine fex

  !*************************
  !compute flux spatial differntial using HLL
  function fhll(n, ni, tt) result(dn)
    use commons
    use nonideal
    use odechem
    use cooling
    use heating
    use crays
#if defined(SELFGRAVITY) || defined(ANALYTICGRAVITY)
    ! use fftw
    use gravity
#endif
    implicit none
    integer,parameter::nall=order*(ncell+2*nghost)
    real*8,intent(in)::n(ncell,neq), ni(ncell,neq), tt
    real*8::ap,am,dn(ncell,neq)
    real*8::Fp(ncell,neq),Fm(ncell,neq)
    real*8::fg(nall,neq),irho(nall)
    real*8::vx(nall),vy(nall),vz(nall)
    real*8::v2(nall),p(nall),f(ncell,neq)
    real*8::lm(nall),lp(nall),ng(nall,neq)
    real*8::pstar(nall),B2(nall),bnorm2(nall)
    real*8::cs2(nall),ab2(nall),cf(nall)
    real*8::dBy(nall),dBz(nall)
    real*8::By(nall),Bz(nall)
    real*8::Flx(nall),Fly(nall),Flz(nall)
    real*8::EADy(nall),EADz(nall)
    real*8::EADBx(nall),adf(nall),eta(nall,3)
    real*8::Tgas(nall),fc(ncell),fh(ncell), Tgas_fake(nall), fion(ncell)
    real*8::p_min(nall)
    integer::i,j,offset
#ifdef PLM
    real*8::rphi(nall,neq)
#endif
#if defined(SELFGRAVITY) || defined(ANALYTICGRAVITY)
    real*8:: g_acc(nall)
#endif

    ! prepare structure with ghosts based on boundary conditions
    ng(:, :) = set_boundaries(n(:, :), ni(:, :), tt)

#if defined(SELFGRAVITY) || defined(ANALYTICGRAVITY)
    g_acc(:) = 0d0
#ifdef SELFGRAVITY
    !get gravitational accelerations (if enabled)
    g_acc(:) = 0d0
    call get_g_fem(ng(:,idx_rho), g_acc(:))
    !call get_g_fft(ng(:,idx_rho),g_acc,0) !!!!NOT TO BE USED FOR THE MOMENT, STILL TO BE TESTED
#endif
    call get_analytic_gravity(g_acc(:))
#endif

#ifdef PLM
    !linear reconstruction with user-defined limiter
    rphi = 0d0
    rphi(2:nall/2-1,:) = (ng(2:nall/2-1,:) - ng(:nall/2-2,:)) / (ng(3:nall/2,:) - ng(2:nall/2-1,:) + 1d-40)

    !now choose slope limiter
    if(slope == 0) then !MINMOD
       rphi = max(0d0, min(rphi, 1d0))
    elseif(slope == 1) then !SUPERBEE
       rphi = max(0d0, min(2*rphi, 1d0), min(rphi, 2d0))
    elseif(slope == 2) then !VAN-ALBADA1
       rphi = (rphi**2 + rphi) / (rphi**2 + 1)
    elseif(slope == 3) then !VAN_LEER
       rphi = (rphi + abs(rphi)) / (abs(rphi) + 1)
    elseif(slope == 4) then !Mono-Central
       rphi = max(0d0, min(2*rphi, 0.5 * (1 + rphi), 2d0))
    elseif(slope == 5) then !Koren
       rphi = max(0d0, min(2*rphi, min((1 + 2 * rphi) / 3d0, 2d0)))
    else
       print *,'no valid slope limiter chosen'
       call abort()
    endif
    offset = nall / 2

    !Right side interpolation
    ng(offset+2:nall-1,:) = ng(2:offset-1,:) + 0.5 * rphi(2:offset-1,:) * (ng(3:offset,:) - ng(2:offset-1,:))

    ! no interpolation on the first/last ghost cells
    ng(offset+1, :) = ng(1, :)
    ng(nall, :) = ng(offset, :)

    !Left side interpolation
    ng(2:offset-1,:) = ng(2:offset-1,:) - 0.5 * rphi(2:offset-1,:) * (ng(3:offset,:) - ng(2:offset-1,:))
#else
    offset = 0
#endif

    !derived quantities from U
    irho(:) = 1d0 / ng(:,idx_rho)
    vx(:) = ng(:,idx_rvx) * irho(:)
    vy(:) = ng(:,idx_rvy) * irho(:)
    vz(:) = ng(:,idx_rvz) * irho(:)
    v2(:) = vx(:)**2 + vy(:)**2 + vz(:)**2
    By(:) = ng(:,idx_By)
    Bz(:) = ng(:,idx_Bz)
    B2(:) = (Bx**2 + ng(:,idx_By)**2 + ng(:,idx_Bz)**2)
    Tgas(:) = (gamma-1d0) * (ng(:,idx_E) - 0.5d0 * ng(:,idx_rho) * v2(:) &
         - 0.5d0 * B2(:) / pi4) / kboltzmann * irho(:) * mu * pmass
    !Tgas(:) = (gamma-1d0) * (ng(:,idx_E) - 0.5d0 * ng(:,idx_rho) * v2(:) &
    !     - 0.5d0 * B2(:) / pi4) / kboltzmann * irho(:) * mu * pmass


    !dBy/dx and dBz/dx, i-centered
    do j=2,nall-1
       dBy(j) = (By(j+1) - By(j-1)) * invdx / 2d0
       dBz(j) = (Bz(j+1) - Bz(j-1)) * invdx / 2d0
    end do

    dBy(1) = 0d0
    dBy(nall) = 0d0
    dBz(1) = 0d0
    dBz(nall) = 0d0

    !Lorentz force, (curl B) x B
    Flx(:) = - (dBy(:)*By(:) + dBz(:)*Bz(:))
    Fly(:) = dBy(:)*Bx
    Flz(:) = dBz(:)*Bx

    ! get resistivity coefficients
    if(switch_resistivity_method=="full") then
       eta(:,:) = getResistivity(ng(:,:), B2(:), Tgas(:), nall)
    elseif(switch_resistivity_method=="tabl") then
       eta(:,:) = getResistivityTable(ng(:,:), B2(:), Tgas(:), nall)
    elseif(switch_resistivity_method=="none") then
       eta(:,:) = 0d0
    else
       print *, "ERROR: resistivity method unknown:", switch_resistivity_method
       stop
    end if
    !EAD: ambipolar EMF, (F x B)/rho_n/rho_i/gammaAD
    adf(:) = eta(:, idx_ambipolar) / B2(:)

    EADy(:) = (Flz(:)*Bx - Flx(:)*Bz(:))
    EADz(:) = (Flx(:)*By(:) - Fly(:)*Bx)

    !EAD x B, x-component only
    EADBx(:) = EADy(:)*Bz(:) - EADz(:)*By(:)

    !pressure
    p(:) = (gamma-1d0)*(ng(:,idx_E) - 0.5*ng(:,idx_rho)*v2(:) - 0.5*B2(:) / pi4)
    pstar(:) = p(:) + 0.5*B2(:) / pi4

    !fluxes (hydro)
    fg(:,idx_rho) = ng(:,idx_rvx)
    fg(:,idx_rvx) = ng(:,idx_rvx)*vx(:) + pstar(:) - Bx**2 / pi4
    fg(:,idx_rvy) = ng(:,idx_rvx)*vy(:) - Bx*ng(:,idx_By) / pi4
    fg(:,idx_rvz) = ng(:,idx_rvx)*vz(:) - Bx*ng(:,idx_Bz) / pi4
    fg(:,idx_By)  = ng(:,idx_By)*vx(:) - Bx*vy(:)
    fg(:,idx_Bz)  = ng(:,idx_Bz)*vx(:) - Bx*vz(:)
    fg(:,idx_E)   = (ng(:,idx_E) + pstar(:))*vx(:) &
         - Bx*(Bx*vx(:) + ng(:,idx_By)*vy(:) + ng(:,idx_Bz)*vz(:)) / pi4

    !ambipolar diffusion corrections, B flux
    fg(:,idx_By) = fg(:,idx_By) + EADz(:) * adf(:)
    fg(:,idx_Bz) = fg(:,idx_Bz) - EADy(:) * adf(:)

    !ambipolar diffusion correction, E flux
    fg(:,idx_E) = fg(:,idx_E) - EADBx(:) * adf(:) / pi4

    ! cosmic rays do not advect
    fg(:, idx_zeta) = 0d0

    !fluxes (chemistry)
    do i=nvar+1,neq
       fg(:,i)  = ng(:,i)*vx(:)
    end do

    !normalized magnetic field squared
    bnorm2(:) = B2(:) * irho(:) / pi4

    !speed of sound squared
    cs2(:) = gamma * p(:) * irho(:)
    ab2(:) = bnorm2(:) + cs2(:)
    !fast magnetosonic speed
    cf(:) = sqrt(0.5*(ab2(:) + sqrt(ab2(:)**2 - 4d0*cs2(:)*Bx**2*irho(:)/pi4)))

    !lambda +/-
    lp(:) = vx(:) + cf(:)
    lm(:) = vx(:) - cf(:)

    !cf(:) = cf(:) + abs(vx)

    !loop on non-ghost cells
    do j=nghost+1,nghost+ncell
       !signal speed at +1/2 interface
       ap = maxval((/0d0, lp(j+1), lp(offset+j)/))
       am = maxval((/0d0, -lm(j+1), -lm(offset+j)/))
       !flux at +1/2 interface
       Fp(j-nghost,:) = ap*fg(offset+j,:) + am*fg(j+1,:) - ap*am &
            * (ng(j+1,:)-ng(offset+j,:))
       Fp(j-nghost,:) = Fp(j-nghost,:)/(ap+am)

       !signal speed at -1/2 interface
       ap = maxval((/0d0, lp(j), lp(offset+j-1)/))
       am = maxval((/0d0, -lm(j), -lm(offset+j-1)/))
       !flux at -1/2 interface
       Fm(j-nghost,:) = ap*fg(offset+j-1,:) + am*fg(j,:) - ap*am &
            * (ng(j,:)-ng(offset+j-1,:))
       Fm(j-nghost,:) = Fm(j-nghost,:)/(ap+am)
    end do

    !spatial differential
    dn(:,:) = -(Fp(:,:)-Fm(:,:)) * invdx

#if defined(SELFGRAVITY) || defined(ANALYTICGRAVITY)
    dn(:,idx_rvx) = dn(:,idx_rvx) + g_acc(nghost+1:nghost+ncell) * n(:,idx_rho)
    dn(:,idx_E) = dn(:,idx_E) + g_acc(nghost+1:nghost+ncell) * n(:,idx_rvx)
#endif

    ! cosmic rays, default is negative
    if(switch_crays) then
       dn(:, idx_zeta) = get_dzeta(n(:,:), dn(:, idx_rho), dn(:, idx_By), dn(:, idx_Bz))
    else
       dn(:, idx_zeta) = 0d0
    end if

    !chemistry
    dn(:,:) = dn(:,:) + odeChemistry(n(:,:), Tgas(nghost+1:nghost+ncell))

    !cooling and heating
    fc(:) = fcool(n(:,:), Tgas(nghost+1:nghost+ncell))
    fh(:) = fheat(n(:,:), Tgas(nghost+1:nghost+ncell))

    dn(:,idx_E) = dn(:,idx_E) - fc(:) + fh(:)

    if(bndry == -1) then
       !fixed boundaries
       dn(1,:) = 0d0
       dn(ncell,:) = 0d0
    endif

  end function fhll

  !*******************
  ! return structure with ghosts cells depending on boundary conditions
  function set_boundaries(n, ni, tt) result(ng)
    use commons
    implicit none
    integer,parameter::nall=order*(ncell+2*nghost)
    real*8,intent(in)::n(ncell, neq), ni(ncell, neq), tt
    real*8::ng(nall, neq)
    integer::j, i, irx, iry, irz

    !copy non-ghost cell into ghost-exted structure
    ng(nghost+1:nghost+ncell,:) = n(:,:)

    if(bndry.eq.0) then
       !set boundary conditions (Dirichlet)
       do j=1,nghost
          ng(j, :) = ni(1, :)
          ng(nghost+ncell+j, :) = ni(ncell, :)
       end do
    elseif(bndry.eq.1) then
       !set outflow boundary conditions
       do j=1,nghost
          ng(j,:) = ng(nghost+1,:)
          ng(nghost+ncell+j,:) = ng(nghost+ncell,:)
       end do
    elseif(bndry.eq.2) then
       !set periodic boundary conditions
       do j=1,nghost
          ng(j,:) = ng(ncell+j,:)
          ng(nghost+ncell+j,:) = ng(nghost+j,:)
       end do
    else
       print *, "ERROR: boundary conditions method unknown", bndry
       stop
    endif

  end function set_boundaries

  !*******************
  !DLSODES time derivative
  function fdlsodes(n,ni,dt) result(nout)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq),dt
    real*8,intent(inout)::ni(ncell, neq)
    real*8::nout(ncell,neq)
    integer::i

    !DLSODES variables
    integer,parameter::meth=2 !1=adam, 2=BDF
    integer::neqa(1),itol,itask,iopt,lrw,liw,mf
    integer::iwork(6462),istate
    real*8::atol(neq*ncell),rtol(neq*ncell),dn(neq*ncell)
    real*8::rwork(int(3e7))
    real*8::tloc,nlin(neq*ncell)

    neqa = neq*ncell !number of eqns
    liw = size(iwork)
    lrw = size(rwork)
    iwork(:) = 0
    rwork(:) = 0d0
    itol = 4 !both tolerances are arrays

    !relative tolerance (non-chemistry)
    rtol(1:nvar*ncell) = 1d-8
    !realtive tolerance (chemistry)
    rtol(nvar*ncell+1:neq*ncell) = 1d-8

    !absolute tolerance (default non-chemistry, but see below)
    atol(1:nvar*ncell) = 1d-8
    !absolute tolerance (chemistry)
    atol(nvar*ncell+1:neq*ncell) = 1d-30

    !absolute tolerance (non-default for selected quantities)
    atol(ncell*(idx_rho-1)+1:ncell*idx_rho) = 1d-30
    atol(ncell*(idx_rvx-1)+1:ncell*idx_rvx) = 1d-25
    atol(ncell*(idx_rvy-1)+1:ncell*idx_rvy) = 1d-25
    atol(ncell*(idx_rvz-1)+1:ncell*idx_rvz) = 1d-25
    atol(ncell*(idx_By-1)+1:ncell*idx_By) = 1d-10
    atol(ncell*(idx_Bz-1)+1:ncell*idx_Bz) = 1d-10
    atol(ncell*(idx_E-1)+1:ncell*idx_E) = 1d-25
    atol(ncell*(idx_zeta-1)+1:ncell*idx_zeta) = 1d-20


    !  Name    Location   Meaning and default value
    !  ------  ---------  -----------------------------------------------
    !  H0      RWORK(5)   Step size to be attempted on the first step.
    !                     The default value is determined by the solver.
    !  HMAX    RWORK(6)   Maximum absolute step size allowed.  The
    !                     default value is infinite.
    !  HMIN    RWORK(7)   Minimum absolute step size allowed.  The
    !                     default value is 0.  (This lower bound is not
    !                     enforced on the final step before reaching
    !                     TCRIT when ITASK = 4 or 5.)
    !  MAXORD  IWORK(5)   Maximum order to be allowed.  The default value
    !                     is 12 if METH = 1, and 5 if METH = 2. (See the
    !                     MF description above for METH.)  If MAXORD
    !                     exceeds the default value, it will be reduced
    !                     to the default value.  If MAXORD is changed
    !                     during the problem, it may cause the current
    !                     order to be reduced.
    !  MXSTEP  IWORK(6)   Maximum number of (internally defined) steps
    !                     allowed during one call to the solver.  The
    !                     default value is 500.
    !  MXHNIL  IWORK(7)   Maximum number of messages printed (per
    !                     problem) warning that T + H = T on a step
    !                     (H = step size).  This must be positive to
    !                     result in a nondefault value.  The default
    !                     value is 10.


    itask = 1
    iopt = 1
    iwork(5) = 0
    iwork(6) = int(1e4)
    MF = 222

    ! store initial conditions
    ninit(:,:) = ni(:,:)

    ! unroll
    do i=1,neq
       nlin(ncell*(i-1)+1:ncell*i) = n(:,i)
    end do

    istate = 1
    tloc = 0d0
    do
       ! call the solver
       CALL DLSODES(fex, neq*ncell, nlin(:), tloc, dt, &
            ITOL, RTOL, ATOL, ITASK, ISTATE, IOPT, RWORK, LRW, IWORK, &
            LIW, JES, MF)

       if(istate==-1) then
          ! reached maximum iterations, continue
          istate = 1
          cycle
       elseif(istate==2) then
          ! succesfull integration, continue
          exit
       elseif(istate==-5) then
          ! wrong sparsity, retry
          istate = 3
          cycle
       else
          ! other problems, stop
          print *,istate
          stop
       end if
    end do

    ! roll
    do i=1,neq
       nout(:,i) = nlin(ncell*(i-1)+1:ncell*i)
    end do

  end function fdlsodes

  !***************************
  !dummy jacobian for DLSODES
  subroutine jes(neqa, tt, n, j, ian, jan, pdj)
    use commons
    implicit none
    integer::neqa, j, ian, jan
    real*8::tt, n(neq), pdj(neq)

    return
  end subroutine jes

end module ode
