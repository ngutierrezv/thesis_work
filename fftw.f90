module fftw
 contains
   SUBROUTINE FFT(DATA,NN,ISIGN)                                     !FFT0010
     !C *** This is the Danielson and Lanczos implementation of the fast      !FFT0020
     !C *** Fourier transform as described in Numerical Recipes, Press et     !FFT0030
     !C *** al in section 12.2.  It has been tested by comparing with         !FFT0040
     !C *** THE ORIGINAL COOLEY-TUKEY TRANSFORM, which is a fortran 4         !FFT0050
     !C *** implementation of the same code.                                  !FFT0060
     !C ***  TRANSFORM(K)=SUM(DATA(J)*EXP(ISIGN*                              !FFT0070
     !C ***  2PI*SQRT(-1)*(J-1)*(K-1)/NN)). SUMMED OVER ALL J                 !FFT0080
     !C *** AND K FROM 1 TO NN. DATA IS IN A ONE-DIMENSIONAL                  !FFT0090
     !C *** COMPLEX ARRAY (I.E.,THE REAL AND IMAGINARY                        !FFT0100
     !C *** PARTS ARE ADJACENT IN STORAGE ,SUCH AS FORTRAN IV                 !FFT0110
     !C *** PLACES THEM) WHOSE LENGTH NN=2**K, K.GE.0 (IF NECESSARY           !FFT0120
     !C *** APPEND ZEROES TO THE DATA). ISIGN IS +1 OR -1. IF A -1            !FFT0130
     !C *** TRANSFORM IS FOLLOWED BY A +1 ONE (OR A +1 BY A -1) THE           !FFT0140
     !C *** ORIGINAL DATA REAPPEAR, MULTIPLIED BY NN. TRANSFORM               !FFT0150
     !C *** VALUES ARE RETURNED IN ARRAY DATA, REPLACING THE INPUT.           !FFT0160
     implicit none
     REAL*8 WR,WI,WPR,WPI,WTEMP,THETA                                  !FFT0170
     REAL*8 :: DATA(2*NN)                                              !FFT0180
     real*8::tempr, tempi
     integer::n, j, i, istep, m, mmax, nn, isign
     N=2*NN                                                            !FFT0190
     J=1                                                               !FFT0200
     DO I=1,N,2                                                        !FFT0210
        IF(J.GT.I)THEN                                                  !FFT0220
           TEMPR=DATA(J)                                                 !FFT0230
           TEMPI=DATA(J+1)                                               !FFT0240
           DATA(J)=DATA(I)                                               !FFT0250
           DATA(J+1)=DATA(I+1)                                           !FFT0260
           DATA(I)=TEMPR                                                 !FFT0270
           DATA(I+1)=TEMPI                                               !FFT0280
        ENDIF                                                           !FFT0290
        M=N/2                                                           !FFT0300
1       IF((M.GE.2).AND.(J.GT.M))THEN                                   !FFT0310
           J=J-M                                                         !FFT0320
           M=M/2                                                         !FFT0330
           GOTO 1                                                        !FFT0340
        ENDIF                                                           !FFT0350
        J=J+M                                                           !FFT0360
     ENDDO                                                             !FFT0370
     !C *** Here begins the Danielson-Lanczos section (outer loop executed    !FFT0380
     !C *** Log2 (NN) times                                                   !FFT0390
     MMAX=2                                                            !FFT0400
2    IF(N.GT.MMAX)THEN                                                 !FFT0410
        ISTEP=2*MMAX                                                    !FFT0420
        THETA=6.28318530717959D0/(ISIGN*MMAX)                           !FFT0430
        WPR=-2*DSIN(0.5D0*THETA)**2                                     !FFT0440
        WPI=DSIN(THETA)                                                 !FFT0450
        WR=1                                                            !FFT0460
        WI=0                                                            !FFT0470
        DO M=1,MMAX,2                                                   !FFT0480
           DO I=M,N,ISTEP                                                !FFT0490
              J=I+MMAX                                                    !FFT0500
              TEMPR=WR*DATA(J)-WI*DATA(J+1)                               !FFT0510
              TEMPI=WR*DATA(J+1)+WI*DATA(J)                               !FFT0520
              DATA(J)=DATA(I)-TEMPR                                       !FFT0530
              DATA(J+1)=DATA(I+1)-TEMPI                                   !FFT0540
              DATA(I)=DATA(I)+TEMPR                                       !FFT0550
              DATA(I+1)=DATA(I+1)+TEMPI                                   !FFT0560
           ENDDO                                                         !FFT0570
           WTEMP=WR                                                      !FFT0580
           WR=WR*WPR-WI*WPI+WR                                           !FFT0590
           WI=WI*WPR+WTEMP*WPI+WI                                        !FFT0600
        ENDDO                                                           !FFT0610
        MMAX=ISTEP                                                      !FFT0620
        GOTO 2                                                          !FFT0630
     ENDIF                                                             !FFT0640
     RETURN                                                            !FFT0650
   END SUBROUTINE FFT                                                               !FFT0660
 end module fftw
