module odechem
contains

  !**************
  function odeChemistry(n, Tgas) result(dn)
    use commons
    use fluxes
    implicit none
    real*8,intent(in)::n(ncell,neq),Tgas(ncell)
    real*8::dn(ncell,neq),flux(ncell,nreacts)
    integer::j

    !get rates
    flux(:,:) = getFlux(n(:,:), Tgas(:))

    !init ODE
    dn(:,:) = 0d0

    !!BEGIN_CHEM
! >>>>>>>>>>>>>>>>>>>>>>>>>>>
! NOTE: This block is auto-generated
! WHEN: 2020-09-01 11:53:47
! CHANGESET: b2eba3bfee45d0064a6dc77b06b4bfbf241d3ca2
! BY: nicol@Quiltras

dn(:,idx_gjj) = -flux(:,5) &
-flux(:,7) &
-flux(:,8) &
+flux(:,12) &
-flux(:,16)

dn(:,idx_gj) = -flux(:,4) &
-flux(:,6) &
+flux(:,7) &
+flux(:,8) &
+flux(:,8) &
+flux(:,11) &
-flux(:,12) &
-flux(:,15) &
+flux(:,16)

dn(:,idx_gqq) = -flux(:,3) &
-flux(:,4) &
-flux(:,5) &
-flux(:,9) &
+flux(:,13)

dn(:,idx_g) = -flux(:,3) &
+flux(:,4) &
+flux(:,5) &
+flux(:,5) &
+flux(:,6) &
+flux(:,6) &
+flux(:,7) &
-flux(:,8) &
+flux(:,10) &
-flux(:,11) &
-flux(:,14) &
+flux(:,15)

dn(:,idx_gq) = +flux(:,3) &
+flux(:,3) &
+flux(:,4) &
-flux(:,6) &
-flux(:,7) &
+flux(:,9) &
-flux(:,10) &
-flux(:,13) &
+flux(:,14)

dn(:,idx_el) = +flux(:,1) &
-flux(:,2) &
-flux(:,13) &
-flux(:,14) &
-flux(:,15) &
-flux(:,16)

dn(:,idx_H2) = -flux(:,1) &
+flux(:,2) &
+flux(:,9) &
+flux(:,10) &
+flux(:,11) &
+flux(:,12)

dn(:,idx_Mgj) = +flux(:,1) &
-flux(:,2) &
-flux(:,9) &
-flux(:,10) &
-flux(:,11) &
-flux(:,12)

    !!END_CHEM

    !from number density to rhoX
    do j=nvar+1,neq
       dn(:,j) = dn(:,j) * mass(j)
    end do

  end function odeChemistry

  !*******************
  subroutine chemical_grid()
    use commons
    implicit none
    integer,parameter::nec=neq-nvar
    integer,parameter::imax=10, jmax=10
    integer::i, j, unit
    real*8::n(nec), ntot, d2g, Tgas, cr_rate, dt

    d2g = 1d-2
    cr_rate = 1d-16
    dt = spy*1d10

    open(newunit=unit, file="chemical_grid.dat", status="replace")
    do i=1,imax
       ntot = 1d1**((i-1)*5./(imax-1))
       do j=1,jmax
          n(:) = 0d0
          Tgas = 1d1**((j-1)*3./(jmax-1))
          print *, ntot, Tgas
          n(idx_H2-nvar) = ntot * mass(idx_H2)
          n(idx_g-nvar) = n(idx_H2-nvar) * d2g
          n(:) =  dochem(n(:), Tgas, cr_rate, dt)
          write(unit, '(99E17.8e3)') sum(n), Tgas, cr_rate, n(:)/sum(n)
       end do
       write(unit, *)
    end do
    close(unit)

  end subroutine chemical_grid

  !*******************
  ! call DLSODES to evolve chemistry only
  ! nin(neq-nvar): chemical species array, g/cm3
  function dochem(nin, Tgas, cr_rate, dt) result(nout)
    use commons
    implicit none
    integer,parameter::nec=neq-nvar+2
    real*8,intent(in)::nin(nec-2), Tgas, dt
    real*8::n(nec), cr_rate, nout(nec-2)
    integer::i

    !DLSODES variables
    integer,parameter::meth=2 !1=adam, 2=BDF
    integer::itol,itask,istate_chem,iopt,lrw,liw,mf
    integer::iwork(6462), neca(1)
    real*8::atol(nec),rtol(nec)
    real*8::rwork(int(1e5))
    real*8::tloc

    ! chemistry (variables, evolved)
    n(1:nec-2) = nin(:)
    ! temperature (variable but not evolved)
    n(nec-1) = Tgas
    ! ionization rate (variable but not evolved)
    n(nec) = cr_rate

    liw = size(iwork)
    lrw = size(rwork)
    iwork(:) = 0
    rwork(:) = 0d0
    itol = 4 !both tolerances are arrays

    !tolerances
    rtol(:) = 1d-8
    atol(:) = 1d-30

    itask = 1
    iopt = 1
    iwork(5) = 0
    iwork(6) = int(1e4)
    MF = 222
    neca(1) = nec

    istate_chem = 1
    tloc = 0d0
    do
       ! call the solver
       CALL DLSODES(fex2, neca, n(:), tloc, dt, &
            ITOL, RTOL, ATOL, ITASK, ISTATE_CHEM, IOPT, RWORK, LRW, IWORK, &
            LIW, jes2, MF)

       if(istate_chem==-1) then
          ! reached maximum iterations, continue
          istate_chem = 1
          cycle
       elseif(istate_chem==2) then
          ! succesfull integration, continue
          exit
       elseif(istate_chem==-5) then
          ! wrong sparsity, retry
          istate_chem = 3
          cycle
       else
          ! other problems, stop
          print *, istate_chem
          stop
       end if
    end do

    ! only get the chemistry as output
    nout(:) = n(1:nec-2)


  end function dochem

  !************************
  ! prepare fex for chemistry to use the normal chemistry ode
  ! input has chemistry+Tgas+zeta and convert to
  ! mhd_vars+chemistry+zeta, where mhd_vars are dummies.
  ! Tgas becomes another array
  subroutine fex2(neqa, tt, n, dn)
    use commons
    implicit none
    integer,parameter::nec=neq-nvar+2
    integer::neqa,i
    real*8::n(nec), dn(nec), nvec(ncell, neq)
    real*8::Tgas(ncell), tt, dnout(ncell, neq)

    nvec(:,:) = 0d0
    ! make ncell copies of the input since odeChemistry is vectorized
    do i=1,ncell
       nvec(i,nvar+1:neq) = n(1:nec-2)
    end do
    ! temperature is the last element of the input
    Tgas(:) = n(nec-1)
    nvec(:, idx_zeta) = n(nec)
    ! compute ode
    dnout(:,:) = odeChemistry(nvec(:,:), Tgas(:))
    ! slice only the first cell, since all the cells are equal
    dn(1:nec-2) = dnout(1, nvar+1:neq)
    ! temperature and crays are constant
    dn(nec-1:nec) = 0d0

  end subroutine fex2

  !***************************
  !dummy jacobian for DLSODES
  subroutine jes2(neqa, tt, n, j, ian, jan, pdj)
    use commons
    implicit none
    integer::neqa, j, ian, jan
    real*8::tt, n(neq), pdj(neq)

    return
  end subroutine jes2

end module odechem
