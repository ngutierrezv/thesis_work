module heating
contains

  ! *******************
  function fheat(n, Tgas)
    use commons
    implicit none
    real*8,intent(in)::n(ncell, neq), Tgas(ncell)
    real*8::fheat(ncell)

    ! total heating erg/cm3/s

    fheat(:) = heat_CR(n(:,:), Tgas(:))

  end function fheat

  ! *******************
  function heat_CR(n, Tgas) result(heat)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq), Tgas(ncell)
    real*8::QGP15(ncell), Htot(ncell), intot(ncell)
    real*8::heat(ncell)
    integer::i

    Htot(:) = n(:,idx_H2) / pmass

    do i=1,ncell
       Htot(i) = min(max(Htot(i), 1d2), 1d10)
    end do

    Htot(:) = log10(Htot(:))

    ! fit of Fig.2, https://arxiv.org/pdf/1502.03380.pdf, eV
    QGP15(:) = 6.88287546406207 &
         + 2.23142125644564 * Htot(:) &
         - 0.532834286679399 * Htot(:)**2 &
         + 0.146965623819987 * Htot(:)**3 &
         - 0.0169067873094841 * Htot(:)**4 &
         + 0.000641832259463311 * Htot(:)**5
    QGP15(:) = QGP15(:) * ev2erg

    !erg/cm3/s
    heat(:) = n(:, idx_zeta) * QGP15(:) * n(:, idx_H2) * imass(idx_H2)

  end function heat_CR

end module heating
