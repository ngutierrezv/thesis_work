# LEMONGRAB, an MHD code to probe shock microphysics

This code is described in Grassi+2019 [https://arxiv.org/abs/1901.00504](https://arxiv.org/abs/1901.00504).      
The test in the paper refers to the commit [bc45e0f](https://bitbucket.org/tgrassi/lemongrab/commits/bc45e0f598abe046de8a4077a255d1b30b35c3aa).

Quick start:
```
python main.py
make
./test
python plot.py
eog plot_*.png &
```
