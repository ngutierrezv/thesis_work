import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import struct
from scipy.integrate import trapz
from scipy import ndimage


def get_rand_field(N, n_low, n_high, k_peak, seed):
    Nhalf = int(N / 2)
    kx = np.fft.fftfreq(N) * N
    kkx, kky, kkz = np.meshgrid(kx, kx, kx)
    kmag = np.sqrt(kkx ** 2 + kky ** 2 + kkz ** 2)
    spec = (kmag / k_peak) ** (n_low - 2)
    for ii in range(len(spec)):
        for jj in range(len(spec[0])):
            valid = kmag[ii, jj, :] > k_peak
            spec[ii, jj, valid] = (kmag[ii, jj, valid] / k_peak) ** (n_high - 2)

    # Set the random seed
    np.random.seed(seed)
    # 3 different ways of creating the random field (default is mode B)
    rmode = 'B'
    # variable default to trigger errors
    phi_k = None
    # ------- Mode A ------#
    # -- Generates the real and imaginary parts as A*cos(theta) and A*sin(theta) --#
    # -- where A follows a normal distribution and theta a uniform distribution  --#
    if rmode == 'A':
        sigma = np.random.standard_normal((N, N, N)) * spec ** 0.5
        theta = np.random.rand(N, N, N) * 2 * np.pi
        sigma = sigma.T
        theta = theta.T
        ar = sigma * np.cos(theta)
        ai = sigma * np.sin(theta)
        # Zero out the 0 and the Nyquist frequencies
        ar[0, :, :] = 0.
        ar[:, 0, :] = 0.
        ar[:, :, 0] = 0.
        ai[0, :, :] = 0.
        ai[:, 0, :] = 0.
        ai[:, :, 0] = 0.
        ar[Nhalf, :, :] = 0.
        ar[:, Nhalf, :] = 0.
        ar[:, :, Nhalf] = 0.
        ai[Nhalf, :, :] = 0.
        ai[:, Nhalf, :] = 0.
        ai[:, :, Nhalf] = 0.
        # Ensures the velocity field is real -> f(k) = f*(-k)
        ar_rev = np.flip(np.flip(np.flip(ar[1:N, 1:N, 1:Nhalf], 2), 1), 0)
        ai_rev = np.flip(np.flip(np.flip(ai[1:N, 1:N, 1:Nhalf], 2), 1), 0)
        ar[1:, 1:, Nhalf + 1:] = ar_rev
        ai[1:, 1:, Nhalf + 1:] = -ai_rev
        # Creates the f(k) complex matrix
        phi_k = ar + ai * 1j
    # ------- Mode B ------#
    # -- Generates the real and imaginary parts as A+jB --#
    # -- where both A and B follow a normal distribution--#
    elif rmode == 'B':
        phi_k = np.random.standard_normal((N, N, N)) + 1j * np.random.standard_normal((N, N, N))
        phi_k *= np.sqrt(spec)
        # Zero out the 0 and the Nyquist frequencies
        phi_k[0, :, :] = 0.
        phi_k[:, 0, :] = 0.
        phi_k[:, :, 0] = 0.
        phi_k[Nhalf, :, :] = 0.
        phi_k[:, Nhalf, :] = 0.
        phi_k[:, :, Nhalf] = 0.

        # Ensures the velocity field is real -> f(k) = f*(-k)
        phi_k_rev = np.flip(np.flip(np.flip(phi_k[1:N, 1:N, 1:Nhalf], 2), 1), 0)
        phi_k[1:, 1:, Nhalf + 1:] = np.conj(phi_k_rev)
    # ------- Mode C ------#
    # -- Generates a normal random field in the time domain and Fourier transform it -- #
    # -- to get a normal random field in the frequency domain (it ensures we have a   -- #
    # -- map of a real field when rescaling with the power spectrum                   -- #
    elif rmode == 'C':
        phi_r = np.random.standard_normal((N, N, N))
        phi_k = np.fft.fftn(phi_r) * np.sqrt(spec)
        del phi_r
    # ------- Unknown mode ------#
    else:
        print("This mode is unknown!")
        quit()

    fx = np.fft.ifftn(phi_k) * N ** 3

    return np.real(fx)


def get_turbulent_velocity(N=256, n_low=10, n_high=-2, k_peak=4, seed=3089):
    if os.path.isfile('./v_%d.dat' % N) and raw_input('Generate new map (y/n)?') != 'y':
        ff = open('./v_%d.dat' % N, 'rb')
        nitem = '%dd' % N ** 3
        v_x = np.array(struct.unpack(nitem, ff.read(N ** 3 * 8)))
        v_y = np.array(struct.unpack(nitem, ff.read(N ** 3 * 8)))
        v_z = np.array(struct.unpack(nitem, ff.read(N ** 3 * 8)))
        v_x = v_x.reshape((N, N, N))
        v_y = v_y.reshape((N, N, N))
        v_z = v_z.reshape((N, N, N))
        return v_x, v_y, v_z

    v_x = get_rand_field(N, n_low, n_high, k_peak, seed)
    v_y = get_rand_field(N, n_low, n_high, k_peak, seed + 17)
    v_z = get_rand_field(N, n_low, n_high, k_peak, seed + 23)
    v = v_x ** 2 + v_y ** 2 + v_z ** 2
    vrms = np.sqrt(np.sum(v) / len(v.flatten()))
    v_x = v_x / vrms
    v_y = v_y / vrms
    v_z = v_z / vrms
    # Velocity found - Now we compute the energy spectrum to check

    ps = np.abs(np.fft.fftn(v_x))**2 + np.abs(np.fft.fftn(v_y))**2 + np.abs(np.fft.fftn(v_z))**2
    ps /= N**6
    kk = np.fft.fftfreq(N) * N
    kx, ky, kz = np.meshgrid(kk, kk, kk)
    k3d = np.sqrt(kx**2 + ky**2 + kz**2)
    kbin = np.linspace(0, N, N)
    hh = np.histogram(k3d.flatten(), bins=kbin, weights=ps.flatten())
    plt.figure()
    plt.step(0.5 * (kbin[1:] + kbin[:-1]), hh[0], where='mid')
    # Theoretical spectrum
    spec = (kbin / k_peak)**n_low
    spec[kbin > k_peak] = (kbin[kbin > k_peak] / k_peak)**n_high
    plt.loglog(kbin, spec / np.pi, 'k')
    plt.ylim(spec[2] / np.pi * 0.1, np.amax(spec / np.pi) * 5)
    plt.savefig('pspec.png')
    plt.clf()
    plt.figure()
    vv = v[:, :, int(N / 2)]
    vv = ndimage.zoom(vv, 2, order=1)
    plt.imshow(vv / vrms, norm=plt.matplotlib.colors.LogNorm(), cmap='Greys', origin='lower')
    plt.colorbar()
    plt.savefig('turb_map.png')
    plt.clf()
    ff = open('v_%d.dat' % N, 'wb')
    ff.write(v_x.flatten())
    ff.write(v_y.flatten())
    ff.write(v_z.flatten())
    ff.close()
    return v_x, v_y, v_z


def map_grid(Lbox, N, vec, gridx):
    np.random.seed(8)
    dx = Lbox / (N - 1)
    ixv = np.floor(vec / dx).astype(int) % N
    iyv = np.floor(np.random.random() * N).astype(int) % N
    izv = np.floor(np.random.random() * N).astype(int) % N
    return np.array(gridx[ixv, iyv, izv])


#######################################
# Get the ridge density of the filament#
#######################################
def get_rho_ridge(M_fil, L_fil, R_flat, p, R_out):
    R = np.linspace(0, R_out, 10000)
    rho = 1. / (1 + (R / R_flat) ** 2) ** (p / 2.)
    mass_to_l = trapz(rho * 2 * np.pi * R, R)
    return M_fil / L_fil / mass_to_l


# Constants
G = 6.673e-08
Msun = 1.989e33
pc = 3.085678e18
AU = 1.5e13
kms = 1e5
mp = 1.67e-24
kb = 1.38e-16
# Parameters
Lbox = 2 * pc
T = 15.0
res = int(sys.argv[1])
Lfil = 0.1 * pc
Mach = 6.0 #2.0
rseed = 3089
Ngrid = 256

# Additional parameters
M2L_fac = 3.0  # Normalised mass to length of the filament
R_flat = 0.033 * pc  # Scale radius of the filament (perpendicular to it)
R_out = 0.4 * pc
p = 2.
mu = 2.

cs = np.sqrt(kb * T / mp / mu)
M2L_crit = 2 * cs ** 2 / G
print("M2L_crit: %g" % (M2L_crit / Msun * pc))

M_total = M2L_fac * M2L_crit * Lfil
rho_0 = get_rho_ridge(M_total, Lfil, R_flat, p, R_out)
print "rho_ridge:", rho_0
rho_bkg = rho_0 / (1 + R_out ** 2 / R_flat ** 2) ** (p / 2.)

# Normalize Lfil
Lfil /= Lbox
x_c = np.arange(0., 1., 1. / res) + 0.5 / res

# Init density
d = np.ones(res) * rho_0 / (1 + (x_c - 0.5) ** 2 * (Lbox / R_flat) ** 2) ** (p / 2.)
d[np.abs(x_c - 0.5) > R_out / Lbox] = rho_bkg

# Init Tgas
Tgas = np.ones_like(d) * T

# fion
fion = np.ones_like(d) * 1e-7

# d2g
d2g = np.ones_like(d) * 1e-2

# crays
crays = np.ones_like(d) * 1e-17

# init velocity
v_rms = Mach * np.sqrt(kb * T / mp / mu)
#v_rms = 0.

vxm, vym, vzm = get_turbulent_velocity(N=Ngrid, n_low=10, n_high=-2, k_peak=6, seed=rseed)
vx = map_grid(Lbox, Ngrid, x_c * Lbox, vxm)
vy = map_grid(Lbox, Ngrid, x_c * Lbox, vym)
vz = map_grid(Lbox, Ngrid, x_c * Lbox, vzm)
print(vx, vy, vz)

# Rescale the velocity field to match the desired Mach number, cm/s
vx *= v_rms
vy *= v_rms
vz *= v_rms

# B field, G
B0 = 10.e-6
By = 9.7437e-6 * (d / d[0]) ** 0.5  #77 degrees from x axis, reference case
Bz = np.ones_like(d) * B0

# Density to cm^-3
dd = d / (mu * mp)

# Averaged velocities
vx_averaged = sum(vx*dd)/sum(dd)
vy_averaged = sum(vy*dd)/sum(dd)
vz_averaged = sum(vz*dd)/sum(dd)

vx -= vx_averaged
vy -= vx_averaged
vz -= vx_averaged

f = open('input.dat', 'w')
for i in range(len(d)):
    f.write(
        '{} {} {} {} {} {} {} {} {} {}\n'.format(dd[i], vx[i], vy[i], vz[i], By[i], Bz[i], Tgas[i],
                                                 crays[i], d2g[i], fion[i]))

vpool = np.append(vx, [vy, vz])
npool = len(vpool)
fout = open("turbulence_pool.dat", "w")
fout.write("%d\n" % npool)
for i in range(npool):
    fout.write("%e\n" % vpool[i])
fout.close()

