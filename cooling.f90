module cooling
  !real*8:: xf(4),Temp(50),ccool(4,50), dxf, dT
  real*8:: nH_k(131), Temp(162),ccool(131,162)
  real*8:: dnH, dT
contains

  !*****************
  function fcool(n,Tgas)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq)
    real*8::fcool(ncell),nH(ncell), lognH(ncell), logTgas(ncell), Tgas_in(ncell),Tgas(ncell)    !fion(ncell)

    !fion(:) = n(:,idx_el)/n(:,idx_H2)
    nH(:) = (n(:,idx_H2) + n(:,idx_Mgj)) / pmass

    lognH(:) = log10(nH(:))
    logTgas(:) = log10(Tgas(:))
    Tgas_in(:) = 1d0    !cooling floor of 10K

    !total cooling erg/cm3/s
    fcool(:) = 10**cool_interp(lognH(:), logTgas(:)) * nH(:)**2 &
         - 10**cool_interp(lognH(:), Tgas_in(:)) * nH(:)**2

  end function fcool

  !*****************
  !Open data to interpolate
  !subroutine load_cooling_table(fcname)
  !  integer:: row, col
  !  character(len=*):: fcname
  !  open(unit=9, file=fcname, status='old', action = 'read')
  !  read(9,*)(xf(col), col=1,4)
  !  read(9,*)(Temp(col), col=1,50)
  !  do row=1,4
  !      read(9,*)(ccool(row,col), col=1,50)
  !  end do
  !  dxf = log10(xf(4)/xf(3))
  !  dT = log10(Temp(4)/Temp(3))
  !end subroutine load_cooling_table

  !*****************
  !Open data to interpolate
  subroutine load_cooling_table(fcname)
    implicit none
    character(len=60)::row_string
    character(len=*):: fcname
    real*8::rout(3)
    integer:: i,j,ios

    open(unit=9, file=fcname, status='old', action = 'read')
    !skips comments, first line and size
    do
       read(9,'(a)') row_string
       if(row_string(1:1)/="#") exit
    end do

    do i=1,size(nH_k)
       do j=1,size(Temp)
          read(9,*,iostat=ios) rout(:)
          Temp(j) = rout(2)
          ccool(i,j) = rout(3)
       end do
       nH_k(i) = rout(1)
       read(9,*,iostat=ios) !skip blanks
       if(ios.ne.0) exit
    end do
    close(9)
    dnH = nH_k(2) - nH_k(1)
    dT = Temp(2) - Temp(1)

  end subroutine load_cooling_table


  !**********************
  !Bilinear interpolation
  function cool_interp(nH, Tgas) result(cool)
    use commons
    implicit none
    real*8,intent(in)::Tgas(ncell),nH(ncell) !fion(ncell)
    real*8 :: cool1, cool2, cool(ncell)
    integer:: i, j, k

    do k = 1, ncell
       !i = floor(log10(fion(k)/xf(1))/dxf) + 1
       i = floor((nH(k) - nH_k(1)) / (dnH + 1d-20)) + 1
       if (i <= 0) then
          i = 1
       else if (i>=size(nH_k)) then
          i = size(nH_k) - 1
       end if

       !j = floor(log10(Tgas(k)/Temp(1))/dT) + 1
       j = floor((Tgas(k)-Temp(1))/dT) + 1
       if (j <= 0) then
          j = 1
       else if (j>=size(Temp)) then
          j = size(Temp) - 1
       end if

       !Lineal interpolation in nH_k
       cool1 = ccool(i, j)*(nH_k(i+1)-nH(k))/(nH_k(i+1)-nH_k(i)) + ccool(i+1,j)*(nH(k)-nH_k(i))/(nH_k(i+1)-nH_k(i))
       cool2 = ccool(i, j+1)*(nH_k(i+1)-nH(k))/(nH_k(i+1)-nH_k(i)) + ccool(i+1, j+1)*(nH(k)-nH_k(i))/(nH_k(i+1)-nH_k(i))

       !Lineal interpolation in T
       cool(k) = cool1*(Temp(j+1)-Tgas(k))/(Temp(j+1)-Temp(j)) + cool2*(Tgas(k)-Temp(j))/(Temp(j+1)-Temp(j))
    end do

  end function cool_interp

  !******************
  !free-free cooling
  function cool_ff(n,Tgas)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq),Tgas(ncell)
    real*8::cool_ff(ncell),ffsum(ncell),ngas(ncell,neq)
    real*8,parameter::gff=1.5d0
    integer::j

    !from rhoX to number density
    do j=nvar+1,neq
       ngas(:,j) = n(:,j)*imass(j)
    end do

    !init sum of ngas*Z**2
    ffsum(:) = 0d0

    !!BEGIN_FFCOOL
! >>>>>>>>>>>>>>>>>>>>>>>>>>>
! NOTE: This block is auto-generated
! WHEN: 2020-09-01 11:53:48
! CHANGESET: b2eba3bfee45d0064a6dc77b06b4bfbf241d3ca2
! BY: nicol@Quiltras

ffsum(:) = ffsum(:) + 1.00000000d+00*ngas(:,idx_Mgj)
ffsum(:) = ffsum(:) + 4.00000000d+00*ngas(:,idx_gqq)
ffsum(:) = ffsum(:) + 1.00000000d+00*ngas(:,idx_gq)
ffsum(:) = ffsum(:) + 1.00000000d+00*ngas(:,idx_gj)
ffsum(:) = ffsum(:) + 4.00000000d+00*ngas(:,idx_gjj)
    !!END_FFCOOL

    !erg/cm3/s
    cool_ff(:) = 1.4d-27*gff*sqrt(Tgas(:)) &
         * ngas(:,idx_el) * ffsum(:)

  end function cool_ff

  !*****************
  function cool_LS85(n,Tgasin) result(cool)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq),Tgasin(ncell)
    real*8::cool(ncell),Tgas(ncell)
    integer::i

    do i=1,ncell
       Tgas(i) = max(Tgasin(i),1d0)
    end do

    cool(:) = 1d1**((log10(Tgas(:))-2d0)/1.5*(29.7-26.)-29.7) &
         * n(:,idx_rho)/pmass/mu

  end function cool_LS85

  !*********************
  ! Smith+93b, https://arxiv.org/pdf/astro-ph/9703171.pdf, eqn.11
  ! erg/s
  function cool_S93(n, Tgasin) result(cool)
    use commons
    implicit none
    real*8,intent(in)::n(ncell,neq), Tgasin(ncell)
    real*8::cool(ncell),Tgas(ncell)
    integer::i

    do i=1,ncell
       Tgas(i) = Tgasin(i)
       if(Tgasin(i)<1d-1) then
          Tgas(i) = 0d0 !max(Tgasin(i), 0d0)
       end if
    end do

    cool(:) = 4.2d-31 * n(:,idx_rho) / pmass / mu * Tgas(:)**3.3

  end function cool_S93

  ! ********************
  ! from KROME
  function cool_H2(n, Tgasin) result(cool)
    use commons
    implicit none
    real*8,intent(in)::n(ncell, neq), Tgasin(ncell)
    real*8::cool(ncell), log3(ncell), Tgas(ncell)
    real*8::HDL(ncell), LDL(ncell), T3(ncell)
    real*8::HDLR, HDLV, logT3(ncell), nH2(ncell)
    integer::i

    do i=1,ncell
       Tgas(i) = min(max(Tgasin(i), 1d1), 1d4)
    end do
    nH2(:) = n(:, idx_H2) / pmass / mu


    T3(:) = Tgas(:) / 1d3
    logT3(:) = log10(T3(:))

    LDL(:) = 1d1**(-2.3962112d1 +2.09433740d0*logt3(:) &
         -.77151436d0*logt3(:)**2 + .43693353d0*logt3(:)**3 &
         -.14913216D0*logt3(:)**4 - .033638326D0*logt3(:)**5) * nH2(:)

    do i=1,ncell
       if(Tgas(i) < 2d3) then
          HDLR = ((9.5e-22 * T3(i)**3.76) / (1d0 + 0.12 * T3(i)**2.1) &
               * exp(-(0.13 / T3(i))**3) + 3d-24 * exp(-0.51 / T3(i))) !erg/s
          HDLV = (6.7e-19 * exp(-5.86 / T3(i)) + 1.6d-18 * exp(-11.7 / T3(i))) !erg/s
          HDL(i)  = HDLR + HDLV !erg/s
       else
          HDL(i) = 1d1**(-2.0584225d1 + 5.0194035 * logt3(i) &
               - 1.5738805 * logt3(i)**2 - 4.7155769 * logt3(i)**3 &
               + 2.4714161 * logt3(i)**4 + 5.4710750 * logt3(i)**5 &
               - 3.9467356 * logt3(i)**6 - 2.2148338 * logt3(i)**7 &
               + 1.8161874 * logt3(i)**8) ! erg/s
       end if
    end do

    cool(:)  = nH2(:) * HDL(:) * LDL(:) / (HDL(:) + LDL(:)) !erg/cm3/s

  end function cool_H2

end module cooling
